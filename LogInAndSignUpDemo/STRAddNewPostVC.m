//
//  STRAddNewPostVC.m
//  SportyTest
//
//  Created by נמרוד בר on 08/09/13.
//
//

#import "STRAddNewPostVC.h"
#import "MMPickerView.h"
#import "STRChooseLocationVC.h"
CGFloat animatedDistance;
@interface STRAddNewPostVC () <CLLocationManagerDelegate, MKMapViewDelegate, PFLogInViewControllerDelegate, PFSignUpViewControllerDelegate, UIAlertViewDelegate, UIGestureRecognizerDelegate,UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UIBarButtonItem *donePickerButton;
@property (weak, nonatomic) IBOutlet UIToolbar *pickerToolbar;

@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (strong,nonatomic) CLLocationManager *loctionManger;
@property (strong,nonatomic) CLGeocoder *geocoder;
@property (strong, nonatomic) PFObject* existPost;
@property (strong, nonatomic) NSArray *activitiesArray;
@property (nonatomic) BOOL allDetailsBeenFilled;
@property (nonatomic) CGPoint originalCenter;
@property (strong, nonatomic) NSArray *numbers;

@property CGFloat animatedDistance;


@end

@implementation STRAddNewPostVC

#pragma mark - Init && Life-cycle
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
        self.inEditMode = NO;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //add gesture recognize for taking keyboard down if backround preesed
    [self addGestureRecognizer];
    
    
    
    [self setNavBarTitle];
    
    [self takeDatePickerOffScreen];
    
    [self setWorkoutSliderProperties];
    
    //set background color
    self.view.backgroundColor=COLORBackgroundWhite;
    
    
    [self setDescreptionLabelProperties];
    
    [self reseatDatePickerDate];
    
    //If in - "Add new post" Mode add Done Button. otherwise add Delete & Done Button
    if (!self.inEditMode)
    {
        [self setNavBarForNewPostVC];
        
        [self getUpdatesOnLocation];
    }
    else
    {
        [self setNavBarForEditMode];
    }
    
    [self initActivityArray];
    
    [self setActivityDateLabel];
    
    //set the did picked location manualy to no
    self.didGotManualLocation=NO;
    
    //check if in edit Mode
    [self checkForEditMode];
    
//    [self setWorkOutSliderProprties];

    self.allDetailsBeenFilled=false;
    [self initActivityDetailsTextBox];
     self.originalCenter = self.mainView.center;
    
    
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    //we check if we got a manual pic from pic location VC
    if(self.didGotManualLocation)
    {
        //make the text color blue
        self.btnForActivityLocationText.titleLabel.textColor=COLORDarkBlue;
        
        //stop getting updates on location
        [self.loctionManger stopUpdatingLocation];
        self.loctionManger =nil;
        
    }
    
    //we will check if we have a loction from the pick location VC
    if(self.currentLocation)
    {
        
        //create pin in location
        // [self createPinOnMapWithStreetDetails:self.currentLocation];
        
        //recantere map on it
        //[self recenterMap:self.currentLocation];
        
        //take location - convert it to string and set it in label
        [self reverseGeocodeLocation: self.currentLocation];
    }
    
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload
{
    [self setLblActivityDescreption:nil];
    //[self setLblSportLoc:nil];
    [self setDatePicker:nil];
    [self setMapView:nil];
    [super viewDidUnload];
}

#pragma mark - Gesture Recognizer setup and delegate


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    
 
    // Disallow recognition of tap gestures in the segmented control.
    CGRect dateDoneButton =  CGRectMake(271, 0, 41, 44);
    CGRect actDoneButton =  CGRectMake(270, 0, 42, 44);
    if (CGRectEqualToRect(touch.view.frame, dateDoneButton) ||(CGRectEqualToRect(touch.view.frame, actDoneButton)))
    {
        return NO;
    }
    return YES;
}

-(void)addGestureRecognizer
{
    //set the tap gesture recognize
    UITapGestureRecognizer* tapRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(viewTapped)];
    [self.view addGestureRecognizer:tapRecognizer];
    tapRecognizer.delegate = self;
}

-(void)viewTapped
{
    [self.view endEditing:YES];
    if (animatedDistance > 0)
    {
        [self EditEnd:self.lblActivityDescreption];
    }
}

#pragma mark - set Views && properties

-(void) setWorkoutSliderProperties
{
    
    //set min/max valus
    self.sliderWorkoutLevel.maximumValue=3;
    self.sliderWorkoutLevel.minimumValue=1;
    
    //set the color of the line
    self.sliderWorkoutLevel.tintColor=COLORRed;
    //set the dot on the slider
    [self.sliderWorkoutLevel setThumbImage:[UIImage imageNamed:@"level_button-Smaller.png"] forState:UIControlStateNormal];
    
    //set the small text under the slider
    [self setSliderLabelsFonts];
}


-(void) setDescreptionLabelProperties
{
    
    //set font for activity descreption
    self.lblActivityDescreption.font=FONTAlefRegular17;
    

}

-(void) setSliderLabelsFonts
{
    self.lblActivityLevelEasy.font=FONTAlefRegular17;
    self.lblActivityLevelMedum.font=FONTAlefRegular17;
    self.lblActivityLevelHard.font=FONTAlefRegular17;
}

-(void) takeDatePickerOffScreen
{
    //set date picker fram outside of screen
    self.datePickerView.frame=CGRectMake(0, self.view.frame.size.height+100, self.datePickerView.frame.size.width, self.datePickerView.frame.size.height);
}

-(void)reseatDatePickerDate
{
    self.datePicker.minimumDate = [NSDate date];
}

-(void)initActivityArray
{
    self.activitiesArray = [[NSArray alloc] initWithObjects:KeyRunning,KeySwimming,KeyWalking,KeyGim,KeyTennis,KeyBasketball,KeySoccer,KeyBicycle,KeySpiritual, KeyOther, nil];
}


-(void)setActivityDateLabel
{
    
    //if we never want the user to post the acrivity without entring a specific date, we wont show him the daye for today
    
    //    //create date format
    //    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    //    //for time
    //    [formatter setDateFormat:@"hh:mm"];
    //    //for date
    //    [formatter setDateFormat:@"MMM dd, YYYY"];
    //    [formatter setDateStyle:NSDateFormatterMediumStyle];
    //
    //
    //    //get the date today in that format
    //    NSString *dateToday = [formatter stringFromDate:[NSDate date]];
    
    
    
    
    
    //set "when" on the button label
    
    //set the font
    self.btnForActivityDateText.titleLabel.font=FONTAlefRegular17;
    self.btnForActivityDateText.titleLabel.textColor=COLORDarkBlue;
    
    //breake line if nedeed
    self.btnForActivityDateText.titleLabel.lineBreakMode=NSLineBreakByCharWrapping;
    self.btnForActivityDateText.clipsToBounds=NO;
    self.btnForActivityDateText.titleLabel.numberOfLines=0;
    
    
    
    //create attributs for text
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    //set the space hight between words
    paragraphStyle.maximumLineHeight = 40.f;
    //makethe text alighn to right
    paragraphStyle.alignment=NSTextAlignmentRight;
    //put the pargraph properties into array
    NSDictionary *attributtes = @{NSParagraphStyleAttributeName : paragraphStyle,};
    //create the atrributed string
    NSAttributedString *activityDateAttributed=[[NSAttributedString alloc]initWithString:KeyWhen attributes:attributtes];
    
    
    //set the attributed text in the label
    [self.btnForActivityDateText setAttributedTitle:activityDateAttributed forState:UIControlStateNormal];
    
    
    
    
    
}


#pragma mark - Location Manager
-(void)getUpdatesOnLocation
{
    
    // if no location manger created- create one
    if(self.loctionManger==nil)
    {
        //init LM
        self.loctionManger= [[CLLocationManager alloc]init];
        //set accuracy of wanted tracking
        self.loctionManger.desiredAccuracy=kCLLocationAccuracyBest;
        
    }
    //start getting updates
    [self.loctionManger startUpdatingLocation];
    //updates on location will get to me
    self.mapView.delegate=self;
    self.loctionManger.delegate=self;
}


#pragma mark - Nav Bar

-(void)setNavBarForNewPostVC
{
   // self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(addPostButtonTapped)];
}

-(void)setNavBarForEditMode
{
    self.navigationItem.rightBarButtonItems  = [NSArray arrayWithObjects:[[UIBarButtonItem alloc]initWithTitle:@"מחק" style:UIBarButtonItemStylePlain target:self action:@selector(deletePost)],  [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(addPostButtonTapped)], nil];
    
}
-(void)setNavBarTitle
{
    self.title=@"עריכת פעילות";
}



#pragma mark - Save && Update post

- (IBAction)btnPublishPost:(id)sender
{
    [self addPostButtonTapped];
}

-(void)updateExistPost
{
    //show activity indicator and dark the screen
    UIActivityIndicatorView *activityView=[[UIActivityIndicatorView alloc]     initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityView.center=self.view.center;
    activityView.tag = 100;
    [activityView setFrame:[UIScreen mainScreen].bounds];
    activityView.backgroundColor = [UIColor blackColor];
    activityView.alpha = 0.8;
    self.view.userInteractionEnabled = NO;
    [activityView startAnimating];
    [self.view addSubview:activityView];
    
    
    //collect the post data that needs to be save
    STRPost* post = [self collectAdData];
    
    //skip updating time if user did not change it
    if (post.activityTime != nil)
    {
        self.existPost[KeyActivityTime] = post.activityTime;
    }
    //skip updating location if user did not change it
    if (post.activityPoint.latitude != 0 || post.activityPoint.longitude != 0)
    {
        self.existPost[KeyActivityLocation] = post.activityPoint;
    }
    
    self.existPost[KeyActivityDesc] = post.activityDesc;
    self.existPost[KeyWorkoutLevel] = post.workoutLevel;
    self.existPost[KeyActivityType] = post.activityType ;
    [self.existPost saveInBackgroundWithTarget:self selector:@selector(postUpdated)];
    
    //    }];
}
-(void)postUpdated
{
    //remove the activity indicator
    [[self.view viewWithTag:100]removeFromSuperview];
    
    //Notify the previous screen to update
    [ [NSNotificationCenter defaultCenter]postNotificationName: @"post_updated"
                                                        object:nil];
    
    
    //go back to user details screen
    [self.navigationController popViewControllerAnimated:YES];
}




- (void)addPostButtonTapped
{
    //check if in edit mode (also means user exist
    if (self.inEditMode)
    {
        //update the current post
        [self updateExistPost];
        
    }
    else //not in edit mode - check if user exist in order to save post
    {
        
            
        
        //if user exist save data
        if ([PFUser currentUser])
        {
            //collect and send to save data
            [self CollectDataForPostsAndSave];
        }
     
        if (![PFUser currentUser]) //if user not exist
        {
            //call the login screen
            
            // Create the log in view controller
            PFLogInViewController *logInViewController = [[PFLogInViewController alloc] init];
            [logInViewController setDelegate:self]; // Set ourselves as the delegate
            
            // Create the sign up view controller
            STRSignUpVC *signUpViewController = [[STRSignUpVC alloc] init];
            [signUpViewController setDelegate:self]; // Set ourselves as the delegate
            
            //what fields should be in the signup
            [signUpViewController setFields:PFSignUpFieldsUsernameAndPassword | PFSignUpFieldsSignUpButton | PFSignUpFieldsDismissButton];
            
            // Assign our sign up controller to be displayed from the login controller
            [logInViewController setSignUpController:signUpViewController];
            
            // Present the log in view controller
            [self presentViewController:signUpViewController animated:YES completion:NULL];
            
        
            }
      
        }
    }
    
    

-(void) CollectDataForPostsAndSave
{
    
    //collect data from fields and GPS
    
    //Create current Post Object
    
    
    STRPost *currentPost = [self collectAdData];
    
    if (self.allDetailsBeenFilled)
    {
    
    //Create thread for the saveing action to run on background
    NSThread *savePostThread=[[NSThread alloc]initWithTarget:self selector:@selector(savePostToDB:) object:currentPost];
    //save it to current user ad list
    [savePostThread start];
    
    //Back to list VC
    [self.navigationController popViewControllerAnimated:YES];
    }
}



#pragma  mark - Sign up Delegate

/// Sent to the delegate when a PFUser is signed up.
- (void)signUpViewController:(PFSignUpViewController *)signUpController didSignUpUser:(PFUser *)user
{
    NSLog(@"we have a user %@", user);
    
    //now after we signed up the user we need to add his gender
    //get the signup VC
    STRSignUpVC *signUpVC = (STRSignUpVC*) signUpController;
    int female= 0;
    //check the selction in the segment
    if (signUpVC.maleFemaleSegmented.selectedSegmentIndex==female)
    {
        [[PFUser currentUser] setObject:keyGenderFemale forKey:keyGender];
        
    }
    else
    {
        //if it's not a female it's a male
        [[PFUser currentUser] setObject:keyGenderMale forKey:keyGender];
        
    }
    
    //save the gender in server
    [[PFUser currentUser] saveInBackground ];
    
    
    [self CollectDataForPostsAndSave];
    
    //dissmis the the signUp VC
    [signUpController dismissViewControllerAnimated:YES completion:nil];
    
    //Back to list VC
    [self.navigationController popViewControllerAnimated:YES];
    
}

// Sent to the delegate to determine whether the sign up request should be submitted to the server.
- (BOOL)signUpViewController:(PFSignUpViewController *)signUpController shouldBeginSignUp:(NSDictionary *)info {
    BOOL informationComplete = YES;
    for (id key in info)
    {
        NSString *field = [info objectForKey:key];
        //if the field is empty
        if (!field || field.length == 0)
        {
            informationComplete = NO;
            break;
        }
        //check if user choose male or female
        STRSignUpVC *signUpVC = (STRSignUpVC*) signUpController;
        int noSelction= (-1);
        if (signUpVC.maleFemaleSegmented.selectedSegmentIndex==noSelction)
        {
            informationComplete = NO;
            break;
        }
        
        
    }
    
    if (!informationComplete) {
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Missing Information", nil) message:NSLocalizedString(@"Make sure you fill out all of the information!", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil] show];
    }
    
    return informationComplete;
}

/// Sent to the delegate when the sign up attempt fails.
- (void)signUpViewController:(PFSignUpViewController *)signUpController didFailToSignUpWithError:(NSError *)error
{
    NSLog(@"FAIL TO LOGIN");
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"You didn't complete the sighn up- try again" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    
    [alertView show];
    
    //dissmis the the login VC
    [signUpController dismissViewControllerAnimated:YES completion:nil];
    
}

// Sent to the delegate to determine whether the log in request should be submitted to the server.
- (BOOL)logInViewController:(PFLogInViewController *)logInController shouldBeginLogInWithUsername:(NSString *)username password:(NSString *)password {
    // Check if both fields are completed
    if (username && password && username.length && password.length) {
        return YES; // Begin login process
    }
    
    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Missing Information", nil) message:NSLocalizedString(@"Make sure you fill out all of the information!", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil] show];
    return NO; // Interrupt login process
}


// Sent to the delegate when a PFUser is logged in.
- (void)logInViewController:(PFLogInViewController *)logInController didLogInUser:(PFUser *)user
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}

// Sent to the delegate when the log in attempt fails.
- (void)logInViewController:(PFLogInViewController *)logInController didFailToLogInWithError:(NSError *)error {
    NSLog(@"Failed to log in...");
}


#pragma mark - TextField Actions && delegates


//shift screen up when start editing text if needed
- (IBAction)editBegan:(UITextField *)sender
{
    [self hideAllKeybaordAndPickers];
    
    
    //make the font color blue
    self.lblActivityDescreption.textColor=COLORDarkBlue;
    
    // Set the distance we want to move
    animatedDistance = sender.frame.origin.y - 220;
    
    [UIView animateWithDuration:0.4 animations:^(void)
     {
         // Get the view of the current view controller
         CGRect viewFrame = self.view.frame;
         
         // Set the y origin of the frame
         viewFrame.origin.y = viewFrame.origin.y - animatedDistance;
         
         [self.view setFrame:viewFrame];
     } completion:^(BOOL finished) {
         [sender becomeFirstResponder];
     }];
    
}



//shift screen back when editing ended
- (IBAction)EditEnd:(UITextField *)sender
{
    [UIView animateWithDuration:0.4 animations:^(void)
     {
         CGRect viewFrame = self.view.frame;
         viewFrame.origin.y += animatedDistance;
         [self.view setFrame:viewFrame];
     }];
    animatedDistance = 0;
}

- (IBAction)txtEditingDone:(UITextField *)sender {
    [self resignFirstResponder];
}

-(void)hideAllKeybaordAndPickers
{
    //Hide keyboard for all textfields
    [self.view endEditing:YES];
    [self EditEnd:self.lblActivityDescreption];
    
    //Hide activity picker view
    [self dissmissChooseActivityView];
    
    //Hide date picker view
    [self doneTapped:nil];
    
}

#pragma mark - Buttons Tapped - Activity info

#pragma mark Change Date
- (IBAction)changeDateTapped:(UIButton *)sender
{
    
    [self hideAllKeybaordAndPickers];
    
    ////////first we take the date picker off screen///////
    
    //Working with UIview from the xib
    CGRect datePickerPosstion =self.datePickerView.frame;
    
    
    //set this posstion to the button
    self.datePickerView.frame =datePickerPosstion;
    
    
    //create the inside screen posstion for the button
    
    CGRect insideScreenPosstion= CGRectMake(0, self.view.frame.size.height-self.datePickerView.frame.size.height, self.datePickerView.frame.size.width, self.datePickerView.frame.size.height);
    
    //Create background
    CGRect basePoint= CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height);
    CGRect background=CGRectMake(0,self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
    
    
    
    
    
    UIView *backgroundView=[UIView new];
    backgroundView.frame= background;
    backgroundView.backgroundColor=[UIColor colorWithRed:100 green:100 blue:100 alpha:0.5];
    
    
    //make the animation of moving from out screen in
    [UIView animateWithDuration:0.5 animations:^{
        backgroundView.frame=basePoint;
        self.datePickerView.frame=insideScreenPosstion;
        
    }
     
     
     ];
    
    
}

- (IBAction)doneTapped:(id)sender
{
    
    //set current time from date picker in current activity time
    self.currentActivityTime=self.datePicker.date;
    
    
    //create the inside screen posstion for the button
    CGRect ouSideScreenPosstion= CGRectMake(0, self.view.frame.size.height, self.datePickerView.frame.size.width, self.datePickerView.frame.size.height);
    
    
    //make the animation of moving outside the screen
    [UIView animateWithDuration:0.5 animations:^{
        
        self.datePickerView.frame=ouSideScreenPosstion;
        
    }
     
     
     ];
    
    
    self.btnForActivityDateText.titleLabel.textColor=COLORDarkBlue;
    
    
    //get the date in date and time format
    NSString *dateToday = [[AppData sharedInstance] dateOrgenizer:self.datePicker.date];
    
    //set the current date to activity label
    
    //create attributs for text
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    //set the space hight between words
    paragraphStyle.maximumLineHeight = 40.f;
    //makethe text alighn to right
    paragraphStyle.alignment=NSTextAlignmentRight;
    //put the pargraph properties into array
    NSDictionary *attributtes = @{NSParagraphStyleAttributeName : paragraphStyle,};
    //create the atrributed string
    NSAttributedString *activityDateAttributed=[[NSAttributedString alloc]initWithString:dateToday attributes:attributtes];
    
    
    //set the attributed text in the label
    [self.btnForActivityDateText setAttributedTitle:activityDateAttributed forState:UIControlStateNormal];
    
    
    
}

#pragma mark change Activity

-(void) displayChooseActivityView
{
    [self hideAllKeybaordAndPickers];
    
    
    [UIView animateWithDuration:0.4 animations:^(void)
     {
         CGRect viewFramec = CGRectMake(0, 100, 320, 380);
         [self.viewChooseActivity setFrame: viewFramec];
     }];
    
    
}

-(void)dissmissChooseActivityView
{
    [UIView animateWithDuration:0.4 animations:^(void)
     {
         CGRect viewFramec = CGRectMake(0, 568, 320, 380);
         [self.viewChooseActivity setFrame: viewFramec];
     }];
}


-(void)activityTypeSelect:(NSString*) selectedString
{
    //set the activity type icon
    if([selectedString isEqualToString:KeyBasketball])
    {
        //if the activity type is basketball - set basketball icon
        [self.btnChooseActivityType setBackgroundImage:[UIImage imageNamed:@"basketball_b.png"] forState:UIControlStateNormal];
    }
    else if ([selectedString isEqualToString:KeyRunning])
    {
        [self.btnChooseActivityType setBackgroundImage:[UIImage imageNamed:@"running_b.png"] forState:UIControlStateNormal];
        
    }
    else if ([selectedString isEqualToString:KeySwimming])
    {
        [self.btnChooseActivityType setBackgroundImage:[UIImage imageNamed:@"swimming_b.png"] forState:UIControlStateNormal];
        
    }
    else if ([selectedString isEqualToString:KeyGim])
    {
        [self.btnChooseActivityType setBackgroundImage:[UIImage imageNamed:@"gym_b.png"] forState:UIControlStateNormal];
        
    }
    else if ([selectedString isEqualToString:KeyWalking])
    {
        [self.btnChooseActivityType setBackgroundImage:[UIImage imageNamed:@"activityPlaceHolder.png"] forState:UIControlStateNormal];
        
    }
    else if ([selectedString isEqualToString:KeyTennis])
    {
        [self.btnChooseActivityType setBackgroundImage:[UIImage imageNamed:@"tennis_b.png"] forState:UIControlStateNormal];
        
    }
    else if ([selectedString isEqualToString:KeySoccer])
    {
        [self.btnChooseActivityType setBackgroundImage:[UIImage imageNamed:@"soccer_b.png"] forState:UIControlStateNormal];
        
    }
    else if ([selectedString isEqualToString:KeyBicycle])
    {
        [self.btnChooseActivityType setBackgroundImage:[UIImage imageNamed:@"bicycle_b.png"] forState:UIControlStateNormal];
        
    }
    else if ([selectedString isEqualToString:KeySpiritual])
    {
        [self.btnChooseActivityType setBackgroundImage:[UIImage imageNamed:@"spiritual_b.png"] forState:UIControlStateNormal];
        
    }
    else if ([selectedString isEqualToString:KeyOther])
    {
        [self.btnChooseActivityType setBackgroundImage:[UIImage imageNamed:@"activityPlaceHolder.png"] forState:UIControlStateNormal];
        
    }
    
    //set the label properties
    //alighn text to right
    self.btnForActivityTypeText.titleLabel.textAlignment=NSTextAlignmentRight;
    //breake line if nedeed
    self.btnForActivityTypeText.titleLabel.lineBreakMode=NSLineBreakByCharWrapping;
    self.btnForActivityTypeText.clipsToBounds=NO;
    //set font parametrs
    self.btnForActivityTypeText.titleLabel.font=FONTAlefBold40;
    self.btnForActivityTypeText.titleLabel.textColor=COLORRed;
    
    
    //convert strung to hebrew
    NSString *activityTypeNameHebrew = [[AppData sharedInstance]convertActivityTypeToHebrew:selectedString];
    
    
    //create attributs for text
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    //set the space hight between words
    paragraphStyle.maximumLineHeight = 40.f;
    //makethe text alighn to right
    paragraphStyle.alignment=NSTextAlignmentRight;
    //put the pargraph properties into array
    NSDictionary *attributtes = @{NSParagraphStyleAttributeName : paragraphStyle,};
    //create the atrributed string
    NSAttributedString *activityNameAttributed=[[NSAttributedString alloc]initWithString:activityTypeNameHebrew attributes:attributtes];
    
    
    //set the attributed text in the label
    [self.btnForActivityTypeText setAttributedTitle:activityNameAttributed forState:UIControlStateNormal];
    
    
}



- (IBAction)btnChooseActivity:(UIButton *)sender {
    
    
    //this will bring the buttons view
    [self displayChooseActivityView];
    
    //    //PickerView with Custom look, with array of strings
    //    UIFont *customFont  = [UIFont fontWithName:@"Palatino-Bold" size:19.0];
    //    NSDictionary *options = @{MMbackgroundColor:[UIColor whiteColor],
    //                              MMtextColor: [UIColor darkTextColor],
    //                              MMtoolbarColor:[UIColor blueColor],
    //                              MMbuttonColor:[UIColor darkGrayColor],
    //                              MMfont: customFont,
    //                              MMvalueY: @5};
    //
    //    [MMPickerView showPickerViewInView:self.view
    //                           withStrings:self.activitiesArray
    //                           withOptions:options
    //                            completion:^(NSString *selectedString)
    //     {
    //                                //selectedString is the return value which you can use as you wish
    //
    //         [self activityTypeSelect: selectedString];
    //
    //
    //
    //     }];
    
}


- (IBAction)btnActivityType:(id)sender
{
    
    //get the name of the activity based on tag number on buttons
    NSString *selectedString;
    if([sender tag]==0)
    {
        //gym
        selectedString=KeyGim;
    }
    else if([sender tag]==1)
    {
        selectedString=KeyBasketball;
    }
    else if([sender tag]==2)
    {
        selectedString=KeyBicycle;
    }
    else if([sender tag]==3)
    {
        selectedString=KeyRunning;
    }
    else if([sender tag]==4)
    {
        selectedString=KeySoccer;
    }
    else if([sender tag]==5)
    {
        selectedString=KeySpiritual;
    }
    else if([sender tag]==6)
    {
        selectedString=KeySwimming;
    }
    else if([sender tag]==7)
    {
        selectedString=KeyTennis;
    }
    else if([sender tag]==8)
    {
        selectedString=KeyWalking;
    }
    else if([sender tag]==9)
    {
        selectedString=KeyOther;
    }
    
    
    
    //make the change on the screen - Label+Icon
    [self activityTypeSelect: selectedString];
    
    
    //take the screen down
    [self dissmissChooseActivityView];
}


#pragma mark change location

- (IBAction)changeLocationTapped:(UIButton *)sender
{
    //בreate choose location vc
    STRChooseLocationVC *chooseLocationVC = [[STRChooseLocationVC alloc]initWithNibName:nil bundle:nil];
    
    chooseLocationVC.currentLocation=self.currentLocation;
    
    [self.navigationController pushViewController:chooseLocationVC animated:YES];
    
}

#pragma mark - Map parts

-(void)setLocationLabel:(CLLocation*) location
{
    if(location!=nil)
    {
        
        //take location - convert it to string and set it in label
        [self reverseGeocodeLocation: location];
    }
    
    
}


-(void) reverseGeocodeLocation: (CLLocation*)location
{
    
    //create a geo coder
    CLGeocoder *geocoder=[[CLGeocoder alloc]init];
    
    
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error)
     {
         if(error ==nil)
         {
             if(placemarks.count>0)
             {
                 CLPlacemark *placemark = [placemarks objectAtIndex:0];
                 
                 MapLocation *annotation = [[MapLocation alloc]init];
                 annotation.street = placemark.thoroughfare;
                 annotation.city = placemark.locality;
                 annotation.state = placemark.administrativeArea;
                 annotation.zip = placemark.postalCode;
                 annotation.coordinate = location.coordinate;
                 
                 
                 //remove all other annotations before adding one
                 //[self.mapView removeAnnotations:[self.mapView annotations]];
                 
                 //add the pin to the map
                 //[self.mapView addAnnotation:annotation];
                 
                 //recantere map on it
                 //[self recenterMap:location];
                 
                 //create a text from the valus
                 NSString *locationInText;
                 //if we don't have a value in subThoroughfare (street number) we won't write it
                 if(placemark.subThoroughfare==NULL)
                 {
                     locationInText = [NSString stringWithFormat:@"%@, %@ ",placemark.thoroughfare ,placemark.locality];
                 }
                 else
                 {
                     locationInText = [NSString stringWithFormat:@"%@ %@, %@ ",placemark.thoroughfare, placemark.subThoroughfare ,placemark.locality];
                 }
                 
                 
                 //set the location label
                 //set the font
                 self.btnForActivityLocationText.titleLabel.font=FONTAlefRegular17;
                 self.btnForActivityLocationText.titleLabel.textColor=COLORDarkBlue;
                 
                 //breake line if nedeed
                 self.btnForActivityLocationText.titleLabel.lineBreakMode=NSLineBreakByCharWrapping;
                 self.btnForActivityLocationText.clipsToBounds=NO;
                 self.btnForActivityLocationText.titleLabel.numberOfLines=0;
                 
                 
                 
                 //create attributs for text
                 NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
                 //set the space hight between words
                 paragraphStyle.maximumLineHeight = 40.f;
                 //makethe text alighn to right
                 paragraphStyle.alignment=NSTextAlignmentRight;
                 //put the pargraph properties into array
                 NSDictionary *attributtes = @{NSParagraphStyleAttributeName : paragraphStyle,};
                 //create the atrributed string
                 NSAttributedString *activityLoactionAttributed=[[NSAttributedString alloc]initWithString:locationInText attributes:attributtes];
                 
                 
                 //set the attributed text in the label
                 [self.btnForActivityLocationText setAttributedTitle:activityLoactionAttributed forState:UIControlStateNormal];
                 
                 self.btnForActivityLocationText.titleLabel.textColor=COLORDarkBlue;
                 
                 //stop getting updates on location
                 [self.loctionManger stopUpdatingLocation];
                 self.loctionManger =nil;
                 
                 
                 
             }
             
             else
             {
                 //if we dont get a placemark array we will show alert daialog
                 UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error translating coordinates" message:@"goecoder did not recognize coordinates" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                 
                 [alert show];
                 
             }
         }
     }
     ];
    
}



-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    //every time we get a new location (moved) we will move the map
    [self recenterMap:userLocation.location];
    
}

-(void) createPinOnMapWithStreetDetails: (CLLocation*)location
{
    
    
    if(!self.geocoder)
    {
        self.geocoder=[[CLGeocoder alloc]init];
        
        [self.geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error)
         {
             if(error ==nil)
             {
                 if(placemarks.count>0)
                 {
                     CLPlacemark *placemark = [placemarks objectAtIndex:0];
                     
                     //want to see what we have in placemark? anable this 2 lines
                     //                    NSDictionary *addressDictionary = placemark.addressDictionary;
                     //                    NSLog(@"%@ ", addressDictionary);
                     
                     
                     
                     
                     //create a pin
                     MapLocation *annotation = [[MapLocation alloc]init];
                     //take all details from placemark and put it on pin
                     annotation.street = placemark.name;
                     annotation.city = placemark.locality;
                     //since we are in israel we won't use "state"
                     //annotation.state = placemark.administrativeArea;
                     annotation.zip = placemark.postalCode;
                     annotation.coordinate = location.coordinate;
                     
                     //remove all other annotations before adding one
                     [self.mapView removeAnnotations:[self.mapView annotations]];
                     
                     
                     //add pin to map
                     [self.mapView addAnnotation:annotation];
                     
                     
                     
                     //set geocoder to nil for next task
                     // self.geocoder=nil;
                     
                 }
                 
                 else
                 {
                     //if we dont get a placemark array we will show alert daialog
                     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error translating coordinates" message:@"goecoder did not recognize coordinates" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                     
                     [alert show];
                     
                 }
             }
         }];
    }
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    //get the last location from array
    self.currentLocation = locations.lastObject;
    
    //recantere map on it
    [self recenterMap:self.currentLocation];
    
    //make an a pin on this location with the street details as subtitle
    [self createPinOnMapWithStreetDetails:self.currentLocation];
    
    //set location label
    [self setLocationLabel:self.currentLocation];
    
    
    
}

-(void) recenterMap: (CLLocation*) location
{
    
    //create a region around the location in size...
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, 500, 500);
    //fit this shape in map view
    MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:viewRegion];
    //set the map region to this new region
    [self.mapView setRegion:adjustedRegion animated:YES];
    
}




#pragma mark - Ad parts

-(void) savePostToDB:(STRPost*)currentPost
{
    
    //Create PFobject from STRPost Properties
    PFObject* post = [[PFObject alloc]initWithClassName:KeyClassPosts];
    
    //set the description of the post
    [post setObject:[NSString stringWithFormat:@"%@", currentPost.activityDesc] forKey: KeyActivityDesc];
    
    //set the user to the PFobject
    [post setObject:currentPost.user forKey:KeyClassUser];
    
    //set the username to the post
    [post setObject:currentPost.user.username forKey:KeyUserName];
    
    //set the user's gender to the post
    [post setObject:currentPost.gender forKey:keyGender];
    
    //set the location of the activity
    [post setObject:currentPost.activityPoint forKey:KeyActivityLocation];
    
    
    //set the activity Type
    [post setObject:currentPost.activityType forKey:KeyActivityType];
    
  
  
    //set the workout Level
    [post setObject:currentPost.workoutLevel forKey:KeyWorkoutLevel];
    
    
    
    //if activityTime is not nil  we will set activityTime key
    if(self.currentActivityTime!=nil)
    {
        //set the time of the activity
        [post setObject:currentPost.activityTime forKey:KeyActivityTime];
        
    }
    
    
    
    //Commit to DB
    [post save];
    
}

-(NSString*) activityTypeEnamToString: (NSInteger) enumType
{
    NSString *string ;
    
    if(enumType==running)
    {
        string= KeyRunning;
    }
    else if(enumType==swimming)
    {
        string= KeySwimming;
    }
    else if(enumType==walking)
    {
        string= KeyWalking;
    }
    else if(enumType==gim)
    {
        string= KeyGim;
    }
    else if(enumType==tennis)
    {
        string= KeyTennis;
    }
    else if(enumType==basketball)
    {
        string=KeyBasketball;
    }
    else if(enumType==soccer)
    {
        string= KeySoccer;
    }
    else if(enumType==bicycle)
    {
        string= KeyBicycle;
    }
    else if(enumType==spiritual)
    {
        string= KeySpiritual;
    }
    else if(enumType==other)
    {
        string=KeyOther;
    }
    
    
    return string;
}

-(NSString*) getActivityType
{

    //convert from hebrew on the label to english for saving in parse
    NSString *activityTypeInEnglish = [[AppData sharedInstance]convertActivityTypeToEnglish:self.btnForActivityTypeText.titleLabel.text];
    
    
    return activityTypeInEnglish;
    
}

-(NSString*) workoutLevelEnamToString: (NSInteger) enumType
{
    NSString *string ;
    
    
    if(enumType==olympic)
    {
        string=@"olympic";
    }
    else if(enumType==athlete)
    {
        string=@"athlete";
    }
    else if(enumType==amature)
    {
        string=@"amature";
    }
    else if(enumType==lazyAss)
    {
        string=@"lazyAss";
    }
    
    
    
    return string;
    
}

-(NSNumber*) getWorkoutLevel
{
    
    //get the tag number from Workout Level segmant
    NSInteger enumWorkoutLevel = self.sliderWorkoutLevel.value;
   
    NSNumber *workOutLevelPrivate=[NSNumber numberWithInteger:enumWorkoutLevel];
    //convert to string
  
    
    return workOutLevelPrivate;
    
}


-(STRPost*) collectAdData
{
    
    //Get the current User
    PFUser* currentUser = [PFUser currentUser];
    
    //create a post object
    STRPost *post=[[STRPost alloc]init];
    
    //set the post' user
    post.user = currentUser;
    
    //set post users gender
    NSDictionary* postData = [currentUser valueForKey:KeyEstimatedData];
    post.gender = postData[keyGender];

        
    //set description from the descrtiption field
    post.activityDesc = self.activtyDetails.text;
    
    //get current location
    CLLocation *currentLocation = self.currentLocation;
    
    //Init the post activity point (PFGeoPoint)
    post.activityPoint = [PFGeoPoint new];
    
    //set user location for activity
    post.activityPoint.latitude=currentLocation.coordinate.latitude;
    post.activityPoint.longitude= currentLocation.coordinate.longitude;
    
    //set activity type
    post.activityType=[self getActivityType];
    //set workout level
    post.workoutLevel=[self getWorkoutLevel];
    
    // add the description to the post
    post.activityDesc=self.activtyDetails.text;
    
    
    //No need to set create time, parse do it automaticly set the time of the creation
    
    
    // Create date from the elapsed time
    //    NSDate *currentDate = [NSDate date];
    //    NSDate *startDate = [NSDate date];
    //    NSTimeInterval timeInterval = [currentDate timeIntervalSinceDate:startDate];
    //    post.postTime=[NSDate dateWithTimeIntervalSince1970:timeInterval];
    
    
    
    //if user didn't set a specific time for activity
    if(self.currentActivityTime==nil)
    {
        //we will send nothing
        post.activityTime =nil;
    }
    else
    {
        //if he set time we will set the specifc time
        post.activityTime =self.currentActivityTime;
    }
    
    if ([self checkForDetails:post])
    {
       
    }
    //verfay all data is correct
      return post;
   
}

-(BOOL)checkForDetails:(STRPost*) post
{
 
    if ([post.activityType isEqualToString:@"במה מתאמנים?"] || post.activityType==nil)
    {
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(KeyAlertTitleMissingDetails, nil) message:NSLocalizedString(KeyAlertMissingActivityType, nil) delegate:nil cancelButtonTitle:NSLocalizedString(KeyAlertMissingActivityDetailsOkButton, nil) otherButtonTitles:nil] show];

        
    }
    else if (post.activityTime==nil)
    {
        
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(KeyAlertTitleMissingDetails, nil) message:NSLocalizedString(KeyAlertMissingActivityDate, nil) delegate:nil cancelButtonTitle:NSLocalizedString(KeyAlertMissingActivityDetailsOkButton, nil) otherButtonTitles:nil] show];
        
    }
    else if (post.activityPoint.latitude==0 || post.activityPoint.longitude==0)
    {
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(KeyAlertTitleMissingDetails, nil) message:NSLocalizedString(KeyAlertMissingActivityLocation, nil) delegate:nil cancelButtonTitle:NSLocalizedString(KeyAlertMissingActivityDetailsOkButton, nil) otherButtonTitles:nil] show];
    }
  
    else
    {
        self.allDetailsBeenFilled=true;
        return true;
    }
    return false;
}



//The VC will apear with the full details of the existing post.
#pragma mark - Edit MODE
-(void)setEditModeWithPost:(PFObject*)post
{
    self.existPost = post;
    self.inEditMode = YES;
}

-(void)checkForEditMode
{
    //Check edit MODE flag to see if need to fill the exist post data.
    if (self.inEditMode)
    {
        [self importPostDetails];
    }
}
-(void)importPostDetails
{
    self.lblActivityDescreption.font=FONTAlefRegular17;
    //self.lblActivityDescreption.textColor=COLORDarkBlue;
    
    NSDictionary* postData = [self.existPost valueForKey:KeyEstimatedData];
    self.btnForActivityTypeText.titleLabel.text = postData[KeyActivityType];
    self.lblActivityDescreption.text = postData[KeyActivityDesc];
    
    
    
    
    NSString *dateOrgeniezed = [[AppData sharedInstance] dateOrgenizer:postData[KeyActivityTime]];
    
    //create attributs for text
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    //set the space hight between words
    paragraphStyle.maximumLineHeight = 40.f;
    //makethe text alighn to right
    paragraphStyle.alignment=NSTextAlignmentRight;
    //put the pargraph properties into array
    NSDictionary *attributtes = @{NSParagraphStyleAttributeName : paragraphStyle,};
    //create the atrributed string
    NSAttributedString *activityDateAttributed=[[NSAttributedString alloc]initWithString:dateOrgeniezed attributes:attributtes];
    
    
    //set the attributed text in the label
    [self.btnForActivityDateText setAttributedTitle:activityDateAttributed forState:UIControlStateNormal];
    
    
    
    
//    self.segWorkoutLevel.selectedSegmentIndex = [self getWorkoutLevelStringAsEnum];
    
    
    //Convert the activity point into location (CLLocation) and set into label
    PFGeoPoint* activityPoint = postData[KeyActivityLocation];
    CLLocation* activityLocation = [[CLLocation alloc]initWithLatitude:activityPoint.latitude
                                                             longitude:activityPoint.longitude];
    [self reverseGeocodeLocation:activityLocation];
    
    
    //doesn't work - no time to fix
    [self createPinOnMapWithStreetDetails:activityLocation];
    
}
/* if every thing work  we can delete thid part
 
 //////////////////////////////////////////////////////////////////////////////////
//-(void)setWorkOutSliderProprties
//{
//    // These number values represent each slider position
//    self.numbers = @[@(1), @(2), @(3)];
//    // slider values go from 0 to the number of values in your numbers array
//    NSInteger numberOfSteps = ((float)[self.numbers count] - 1);
//    self.sliderWorkoutLevel.maximumValue = numberOfSteps;
//    self.sliderWorkoutLevel.minimumValue = 0;
//    self.sliderWorkoutLevel.value=1;
//    
//    // As the slider moves it will continously call the -valueChanged:
//    self.sliderWorkoutLevel.continuous = YES; // NO makes it call only once you let go
//    [self.sliderWorkoutLevel addTarget:self
//               action:@selector(valueChanged:)
//     forControlEvents:UIControlEventValueChanged];
//}
 
 /////////////////////////////////////////////////////////////////////////////////
 */
- (void)valueChanged:(UISlider *)sender {
    // round the slider position to the nearest index of the numbers array
    NSUInteger index = (NSUInteger)(self.sliderWorkoutLevel.value);
    [self.sliderWorkoutLevel setValue:index animated:NO];

}

- (IBAction)dragEndedWithSlider:(UISlider *)sender {
    CGFloat roundedValue = roundf(sender.value);
    [sender setValue:roundedValue animated:YES];
}


-(void)deletePost
{
    UIAlertView* deleteAlert = [[UIAlertView alloc]initWithTitle:@"מחיקת פוסט" message:@"הפוסט יימחק לעד פרט לנתונים שאנחנו שומרים עליכם כמובן, האם למחוק ?" delegate:self cancelButtonTitle:@"ביטול" otherButtonTitles:@"מחיקה", nil];
    [deleteAlert show];
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        NSLog(@"delete");
        [self.existPost deleteInBackground];
        [self.navigationController popViewControllerAnimated:YES];
    }
}
-(void)initActivityDetailsTextBox
{
    self.activtyDetails.text = @"עוד פרטים על הפעילות...";
    self.activtyDetails.textColor = [UIColor lightGrayColor];
    self.activtyDetails.font =FONTAlefRegular17;
    self.activtyDetails.delegate = self;
}
- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    self.activtyDetails.text = @"";
    self.activtyDetails.textColor = COLORDarkBlue;
    return YES;
}

-(void) textViewDidChange:(UITextView *)textView
{
    
    if(self.activtyDetails.text.length == 0){
        self.activtyDetails.textColor = [UIColor lightGrayColor];
        self.activtyDetails.text = @"עוד פרטים על הפעילות...";
        [self.activtyDetails resignFirstResponder];
    }
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    for (UIView * txt in self.view.subviews){
        if ([txt isKindOfClass:[UITextField class]] && [txt isFirstResponder]) {
            [txt resignFirstResponder];
        }
    }
}
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    self.mainView.center = CGPointMake(self.originalCenter.x,self.originalCenter.y-100);
   

}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    self.mainView.center = CGPointMake(self.originalCenter.x,self.originalCenter.y+44);
//    self.mainView.center = self.originalCenter;

}

@end
