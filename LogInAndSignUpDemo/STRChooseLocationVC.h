//
//  STRChooseLocationVC.h
//  Sporty
//
//  Created by gonzo bonzo on 3/9/14.
//
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "MapLocation.h"
#import "STRAddNewPostVC.h"

@interface STRChooseLocationVC : UIViewController <MKMapViewDelegate, UIGestureRecognizerDelegate>

@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UISearchBar *serachBar;

@property (strong,nonatomic) CLLocation *currentLocation;
@property (strong, nonatomic) NSMutableArray *matchingItems;

@end
