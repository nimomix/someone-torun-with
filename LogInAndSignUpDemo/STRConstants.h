//
//  STRConstants.h
//  Sporty
//
//  Created by Ariel Peremen on 10/25/13.
//
//

#import <Foundation/Foundation.h>

@interface STRConstants : NSObject

extern NSString *const MyThingNotificationKey;

//app parameters
extern int const RadiosForPostSearch;

//app key for valus
extern NSString *const KeyUserName;

extern NSString *const KeyActivityTime;
extern NSString *const KeyActivityType;
extern NSString *const KeyActivityDesc;

extern NSString *const KeyPhoneNumber;
extern NSString *const KeyWorkoutLevel;
extern NSString *const KeyActivityLocation;

//server keys
extern NSString *const KeyObjectId;
extern NSString *const KeyServerData;
extern NSString *const KeyCreatedAt;
extern NSString *const KeyClassUser;
extern NSString *const KeyClassPosts;
extern NSString *const KeyClassAnswers;
extern NSString *const KeyEstimatedData;
extern NSString *const keyPost;
extern NSString *const keyAnsweredBy;
extern NSString *const keyGender;
extern NSString *const keyGenderFemale;
extern NSString *const keyGenderMale;
extern NSString *const keyProfilePicture;


extern NSString *const KeyMenuHome;
extern NSString *const KeymenuMessegesBox;
extern NSString *const KeyMenuUserDetails;
extern NSString *const KeyMenuRunning;
extern NSString *const KeyMenuSwimming;
extern NSString *const KeyMenuGim;
extern NSString *const KeyMenu;
extern NSString *const KeyMenuWalking;
extern NSString *const KeyMenuTennis;
extern NSString *const KeyMenuBasketball;
extern NSString *const KeyMenuSoccer;
extern NSString *const KeyMenuBicycle;
extern NSString *const KeyMenuSpiritual;




extern NSString *const KeyRunning;
extern NSString *const KeySwimming;
extern NSString *const KeyGim;
extern NSString *const KeyWalking;
extern NSString *const KeyTennis;
extern NSString *const KeyBasketball;
extern NSString *const KeySoccer;
extern NSString *const KeyBicycle;
extern NSString *const KeySpiritual;
extern NSString *const KeyOther;

extern NSString *const KeyMenuGroups;
extern NSString *const KeyMenuOther;
extern NSString *const KeyMenuAboutUs;
extern NSString *const KeyMenuReport;
extern NSString *const KeyMenuLogOut;
//extern NSString *const KeyclassAnswers;


//Distance from current post

extern NSString *const KeyDistanceMoreThenHour;
extern NSString *const KeyDistanceDriving;
extern NSString *const KeyDistanceBicyle;
extern NSString *const KeyDistanceWalking;
extern NSString *const KeyDistanceHere;

//When labels are empty

extern NSString *const KeyNoDetails;
extern NSString *const KeyNoPartnersCell;
extern NSString *const KeyAlertTitleMissingDetails;
extern NSString *const KeyAlertMissingActivityLocation;
extern NSString *const KeyAlertMissingActivityDate;
extern NSString *const KeyAlertMissingActivityType;
extern NSString *const KeyAlertMissingActivityDetailsOkButton;
extern NSString *const KeyWhen;




@end
