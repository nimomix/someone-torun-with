//
//  STRChooseLocationVC.m
//  Sporty
//
//  Created by gonzo bonzo on 3/9/14.
//
//

#import "STRChooseLocationVC.h"

@interface STRChooseLocationVC () <UISearchBarDelegate>

@property (strong,nonatomic) CLLocationManager *loctionManger;
@property (strong,nonatomic) CLGeocoder *geocoder;

@property (strong,nonatomic)  MapLocation *currentPin;


@end

@implementation STRChooseLocationVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    //set map on current possion
    if(self.currentLocation!=nil)
    {    
    //recantere map on it
    [self recenterMap:self.currentLocation];
    
    //make an a pin on this location with the street details as subtitle
    [self createPinOnMapWithStreetDetails:self.currentLocation];
    }
    
    //get updates from searchBar
    self.serachBar.delegate=self;
    
    //set long press gesture to put a pin
    [self setMapGestures];
    
    //set nav bar button
    [self setNavigtionBarButton];


   
    
}

-(void)setMapGestures
{
    //set long press gesture to put a pin
    UILongPressGestureRecognizer *longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPressGesture:)];
    //add gesture to map
    [self.mapView addGestureRecognizer:longPressGesture];
}

-(void) setNavigtionBarButton
{
    //create the right "DONE" bitton
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonTapped)];
    
    //set right nav bar button
    self.navigationItem.rightBarButtonItem=rightButton;
}

-(void) doneButtonTapped
{
    
    //NSLog(@"%@",[self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2]);
       
    //if we have a pin in map
    if(self.currentPin!=nil)
    {
        //get the VC that called me
        STRAddNewPostVC *strAddnewPostVC =[self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2];
        
        
        //get the pin location and convert it to loaction
        CLLocation *location = [[CLLocation alloc]initWithLatitude:self.currentPin.coordinate.latitude longitude:self.currentPin.coordinate.longitude];
        
        //set it's location to pin location
        strAddnewPostVC.currentLocation=location;
        
        //set the BOOL that says we have picked loaction manualy
        strAddnewPostVC.didGotManualLocation=YES;
    }
    
    //Back to add new post VC
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)handleLongPressGesture:(UIGestureRecognizer*)sender {
    // This is important if you only want to receive one tap and hold event
    if (sender.state == UIGestureRecognizerStateEnded)
    {
        //[self.mapView removeGestureRecognizer:sender];
        //NSLog(@"remove gestureHandler");
    }
    else
    {
        //remove all other annotations
        [self.mapView removeAnnotations:[self.mapView annotations]];
        
        // Here we get the CGPoint for the touch and convert it to latitude and longitude coordinates to display on the map
        CGPoint point = [sender locationInView:self.mapView];
        CLLocationCoordinate2D locCoord = [self.mapView convertPoint:point toCoordinateFromView:self.mapView];
        
        
        // Then all you have to do is create the annotation and add it to the map

        
        self.currentPin = [[MapLocation alloc] init];
        
       
        
        self.currentPin.coordinate = locCoord;
        [self.mapView addAnnotation:self.currentPin];
        
         CLLocation *location = [[CLLocation alloc]initWithLatitude:locCoord.latitude longitude:locCoord.longitude];
        
        [self  reverseGeocodeLocation:location];
        
        [self recenterMap:location];
        
        
        NSLog(@"added manual pin");
     
    }
}

- (IBAction)mapTapped:(UITapGestureRecognizer *)sender
{
    [self.view endEditing:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self.view endEditing:YES];
    
    [self.mapView removeAnnotations:[self.mapView annotations]];
    
    [self performSearch];
    
}

- (void) performSearch
{
    //create a search request
    MKLocalSearchRequest *request = [[MKLocalSearchRequest alloc] init];
    //get text from search field - set it ti request
    request.naturalLanguageQuery = self.serachBar.text;
    NSLog(@"Searching: %@",self.serachBar.text);
    request.region = self.mapView.region;
    
    self.matchingItems = [[NSMutableArray alloc] init];
    
    MKLocalSearch *search = [[MKLocalSearch alloc]initWithRequest:request];
    
    [search startWithCompletionHandler:^(MKLocalSearchResponse
                                         *response, NSError *error) {
        if (response.mapItems.count == 0)
            NSLog(@"No Matches");
        else
            for (MKMapItem *item in response.mapItems)
            {
                [self.matchingItems addObject:item];
                self.currentPin = [[MapLocation alloc]init];
                self.currentPin.coordinate = item.placemark.coordinate;
                //take all details from placemark and put it on pin
                self.currentPin.street = item.name;
//                self.currentPin.city = item.placemark;
                
                //self.currentPin.title = item.name;
                [self.mapView addAnnotation:self.currentPin];
                
                CLLocation *location = [[CLLocation alloc]initWithLatitude:item.placemark.coordinate.latitude longitude:item.placemark.coordinate.longitude];
                
                
                [self recenterMap:location];
                
                NSLog(@"%lu Matches!",(unsigned long)response.mapItems.count);
            }
    }];
}

-(void) reverseGeocodeLocation: (CLLocation*)location
{
    
    //create a geo coder
    CLGeocoder *geocoder=[[CLGeocoder alloc]init];
    
    
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error)
     {
         if(error ==nil)
         {
             if(placemarks.count>0)
             {
                 CLPlacemark *placemark = [placemarks objectAtIndex:0];
                 
                 self.currentPin = [[MapLocation alloc]init];
                 self.currentPin.street = placemark.thoroughfare;
                 self.currentPin.city = placemark.locality;
                 self.currentPin.state = placemark.administrativeArea;
                 self.currentPin.zip = placemark.postalCode;
                 self.currentPin.coordinate = location.coordinate;
                 
                 //remove all other annotations before adding one
                 [self.mapView removeAnnotations:[self.mapView annotations]];
                 
                 //add the new annotation
                 [self.mapView addAnnotation:self.currentPin];
                 
                 //create a text from the valus
                 //NSString *locationInText = [NSString stringWithFormat:@"%@, %@ ",placemark.thoroughfare ,placemark.locality];
                 
                 //set the location label
                 //self.lblActivityLocation.text=locationInText;
                 
                 //stop getting updates on location
                 [self.loctionManger stopUpdatingLocation];
                 self.loctionManger =nil;
                 
                 
                 
             }
             
             else
             {
                 //if we dont get a placemark array we will show alert daialog
                 UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error translating coordinates" message:@"goecoder did not recognize coordinates" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                 
                 [alert show];
                 
             }
         }
     }
     ];
    
}



-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    //every time we get a new location (moved) we will move the map
    [self recenterMap:userLocation.location];
    
}

-(void) createPinOnMapWithStreetDetails: (CLLocation*)location
{
    
    
    if(!self.geocoder)
    {
        self.geocoder=[[CLGeocoder alloc]init];
        
        [self.geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error)
         {
             if(error ==nil)
             {
                 if(placemarks.count>0)
                 {
                     CLPlacemark *placemark = [placemarks objectAtIndex:0];
                     
                     //want to see what we have in placemark? anable this 2 lines
                     //                    NSDictionary *addressDictionary = placemark.addressDictionary;
                     //                    NSLog(@"%@ ", addressDictionary);
                     
                     
                     
                     
                     //create a pin
                     self.currentPin = [[MapLocation alloc]init];
                     //take all details from placemark and put it on pin
                     self.currentPin.street = placemark.name;
                     self.currentPin.city = placemark.locality;
                     //since we are in israel we won't use "state"
                     //annotation.state = placemark.administrativeArea;
                     self.currentPin.zip = placemark.postalCode;
                     self.currentPin.coordinate = location.coordinate;
                     
                     
                     
                     
                     //add pin to map
                     [self.mapView addAnnotation:self.currentPin];
                     
                     
                     
                     //set geocoder to nil for next task
                     // self.geocoder=nil;
                     
                 }
                 
                 else
                 {
                     //if we dont get a placemark array we will show alert daialog
                     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error translating coordinates" message:@"goecoder did not recognize coordinates" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                     
                     [alert show];
                     
                 }
             }
         }];
    }
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    //get the last location from array
    self.currentLocation = locations.lastObject;
    
    //recantere map on it
    [self recenterMap:self.currentLocation];
    
    //make an a pin on this location with the street details as subtitle
    [self createPinOnMapWithStreetDetails:self.currentLocation];
    
    //set location label
    //[self setLocationLabel:self.currentLocation];
    
    
    
}

-(void) recenterMap: (CLLocation*) location
{
    
    //create a region around the location in size...
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, 200, 200);
    //fit this shape in map view
    MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:viewRegion];
    //set the map region to this new region
    [self.mapView setRegion:adjustedRegion animated:YES];
    
}


@end
