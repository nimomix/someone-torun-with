//
//  STRLoginVC.m
//  Sporty
//
//  Created by gonzo bonzo on 4/7/14.
//
//

#import "STRSignUpVC.h"
#import "MyLogInViewController.h"
#import "MySignUpViewController.h"

@interface STRSignUpVC ()

@end

@implementation STRSignUpVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    //set the color of the background
    [self.signUpView setBackgroundColor:[UIColor brownColor]];
    
    // Set field text color
    [self.signUpView.usernameField setTextColor:[UIColor blackColor ]];
    [self.signUpView.passwordField setTextColor:[UIColor blackColor ]];

    //set background color for the fields
    [self.signUpView.passwordField setBackgroundColor:[UIColor redColor]];
    [self.signUpView.usernameField setBackgroundColor:[UIColor greenColor]];
    
    //hide the email field
    [self.signUpView.emailField setHidden:YES];
    
    //create array with two objects
    NSArray *itemArray = [NSArray arrayWithObjects: @"", @"", nil];
    //add array to the segment
    self.maleFemaleSegmented =[[UISegmentedControl alloc] initWithItems:itemArray];
    //set segment frame
    self.maleFemaleSegmented.frame=CGRectMake(36, 326, 246, 40);
    //set segment color
    self.maleFemaleSegmented.tintColor=[UIColor blackColor];
    //set the background of the segment
    [self.maleFemaleSegmented setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"IMG_male_female_signUp"]]];
    //add sagment to our view
    [self.signUpView addSubview:self.maleFemaleSegmented];
    
    
    //set the frame off the dissmiss button
    [self.signUpView.dismissButton setFrame:CGRectMake(36, 100, 40, 40)];
    //hide nav bar
    self.navigationController.navigationBar.hidden=YES;
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    

    [self.signUpView.signUpButton setFrame:CGRectMake(35.0f, 385.0f, 250.0f, 40.0f)];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




@end
