//
//  WIAppData.m
//  iTrade
//
//  Created by Ariel Peremen on 8/11/13.
//  Copyright (c) 2013 Wabbit. All rights reserved.
//

#import "AppData.h"


@interface AppData () <CLLocationManagerDelegate>

@property (strong, nonatomic) CLLocationManager* locationManager;
@property (strong, nonatomic) CLLocation* currentLocation;


@end

static AppData* staticInstance;


@implementation AppData



//creating the singletone

+(AppData*)sharedInstance
{
    if (staticInstance==nil)
    {
        staticInstance=[AppData new];
        staticInstance.postsAnswered = [NSMutableArray array];
    }
    return staticInstance;
}

- (id)init
{
    self.currentFilterActivityType = @"ALL";
    return self;
}





-(CLLocation*) getCurrentLocation
{
    //Get the current location of the user to show the close-Locations Posts
    if (self.locationManager == nil) {
        self.locationManager = [[CLLocationManager alloc]init];
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest ;
    }
    [self.locationManager startUpdatingLocation];
    self.locationManager.delegate = self;
    
    self.currentLocation = self.locationManager.location;
    
    return self.currentLocation;
}

-(NSString*) convertActivityTypeToHebrew :(NSString*) activityTypeName
{
    NSString *activityTypeInHebrew;
    
    if([activityTypeName isEqualToString:KeyBasketball])
    {
        //if the activity type is basketball convert it to hebrew
        activityTypeInHebrew=KeyMenuBasketball;
    }
    else if ([activityTypeName isEqualToString:KeyRunning])
    {
        activityTypeInHebrew=KeyMenuRunning;
    }
    else if ([activityTypeName isEqualToString:KeySwimming])
    {
        activityTypeInHebrew=KeyMenuSwimming;
    }
    else if ([activityTypeName isEqualToString:KeyGim])
    {
        activityTypeInHebrew=KeyMenuGim;
    }
    else if ([activityTypeName isEqualToString:KeyWalking])
    {
        activityTypeInHebrew=KeyMenuWalking;
    }
    else if ([activityTypeName isEqualToString:KeyTennis])
    {
        activityTypeInHebrew=KeyMenuTennis;
    }
    else if ([activityTypeName isEqualToString:KeySoccer])
    {
        activityTypeInHebrew=KeyMenuSoccer;
    }
    else if ([activityTypeName isEqualToString:KeyBicycle])
    {
        activityTypeInHebrew=KeyMenuBicycle;
    }
    else if ([activityTypeName isEqualToString:KeySpiritual])
    {
        activityTypeInHebrew=KeyMenuSpiritual;
    }
    else if ([activityTypeName isEqualToString:KeyOther])
    {
        activityTypeInHebrew=KeyMenuOther;
    }

        return activityTypeInHebrew;
}



-(NSString*) convertActivityTypeToEnglish :(NSString*) activityTypeName
{
    NSString *activityTypeInHebrew;
    
    if([activityTypeName isEqualToString: KeyMenuBasketball])
    {
        //if the activity type is basketball convert it to hebrew
        activityTypeInHebrew=KeyBasketball;
    }
    else if ([activityTypeName isEqualToString:KeyMenuRunning])
    {
        activityTypeInHebrew= KeyRunning;
    }
    else if ([activityTypeName isEqualToString:KeyMenuSwimming])
    {
        activityTypeInHebrew= KeySwimming;
    }
    else if ([activityTypeName isEqualToString:KeyMenuGim])
    {
        activityTypeInHebrew= KeyGim;
    }
    else if ([activityTypeName isEqualToString:KeyMenuWalking])
    {
        activityTypeInHebrew= KeyWalking;
    }
    else if ([activityTypeName isEqualToString:KeyMenuTennis])
    {
        activityTypeInHebrew= KeyTennis;
    }
    else if ([activityTypeName isEqualToString:KeyMenuSoccer])
    {
        activityTypeInHebrew= KeySoccer;
    }
    else if ([activityTypeName isEqualToString:KeyMenuBicycle])
    {
        activityTypeInHebrew= KeyBicycle;
    }
    else if ([activityTypeName isEqualToString:KeyMenuSpiritual])
    {
        activityTypeInHebrew= KeySpiritual;
    }
    else if ([activityTypeName isEqualToString:KeyMenuOther])
    {
        activityTypeInHebrew= KeyOther;
    }
    
    return activityTypeInHebrew;
}


-(NSString*) dateOrgenizer: (NSDate*) dateToOrgenize
{
    //creat the format for the time stamp
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"'ב-'dd.MM 'בשעה' HH:mm"];
    
    //******* in the futur- compere the date with the current date and retrive a word ("היום״ ״אתמול)********
    
    //create the time stamp
    //NSDate * now = [NSDate date];
    
    
    //Change the time stamp to a string
    NSString *formattedDateString = [dateFormatter stringFromDate:dateToOrgenize];
    return formattedDateString;
}

-(NSString*) convertDateToRelativeTime: (NSDate*) activityFullDate
{
    
    
    //creat the format for the time stamp
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    //Change the time stamp to a string
    NSString *formatedTImeString = [dateFormatter stringFromDate:activityFullDate];
    
    //create calender object
    NSCalendar *cal = [NSCalendar currentCalendar];
    //get today's full date
    NSDate *todaysFullDate = [NSDate date];
    
    //get only the year month and day from todays date
    NSDateComponents *todaysComps = [cal components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:todaysFullDate];
    //create date only from those commponets
    NSDate *today = [cal dateFromComponents:todaysComps];
    
    
    //get only the year month and day from activity date
    NSDateComponents *activityDateComps = [cal components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:activityFullDate];
    //create activity date only from those commponets
    NSDate *activityDate = [cal dateFromComponents:activityDateComps];

    
    //create the string to send back
    NSString *relativeTimeString;
    
    //if it's the same day
    if([activityDate isEqualToDate:today])
    {
        //say it's today and add time
        relativeTimeString=[NSString stringWithFormat:@"היום ב-%@",formatedTImeString];
    }
    else
    {
        //compare how many days between now and the activity
        NSDateComponents *activityDateComps = [cal components:( NSDayCalendarUnit) fromDate:today toDate:activityDate options:0];
        //get only the days from activity date
        NSInteger days = [activityDateComps day];
     
        if(days==1)
        {
             //if it's one day from today say it's tommarow and add time
            relativeTimeString=[NSString stringWithFormat:@"מחר ב-%@",formatedTImeString];
        }
        else
        {
            //if it's more then one day from now say how many days
           relativeTimeString=[NSString stringWithFormat:@"בעוד %ld ימים",(long)days];
        }
        
        
    }

    return relativeTimeString;
    
}
-(void) saveDataToDisk
{
    //prepare the user defults
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    //convert the data of users array to DATA and saves to archive
    NSData *answers = [NSKeyedArchiver archivedDataWithRootObject:self.postsAnswered];
    
    //save in archive Users
    [defaults setObject:answers forKey:@"answers"];
    
    //Action
    [defaults synchronize];
}

-(void) loadDataFromDisk
{
    
    //get the user defults
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    //load the archived users array from disc
    NSData  *answers =[defaults objectForKey:@"answers"];
    
    //unarchive the users array from data and set it to Users array property.
    NSMutableArray *answersArray= [NSKeyedUnarchiver unarchiveObjectWithData:answers];
    
    if (answersArray!=nil)
    {
        self.postsAnswered = answersArray;
    }
    
}

-(void)getProfileImageFromServer :(NSString*)objectId andUiImage:(UIImageView*) viewForImage
{
    //first we set a generic pic so if no profile pic- will have somthing
    viewForImage.image=[UIImage imageNamed:@"IMG_User_Runner.png"];
    
    //create a query on user class
    PFQuery *query = [PFUser query];
    //get the object that has this object id
    [query getObjectInBackgroundWithId:objectId block:^(PFObject *user, NSError *error)
     {
          if (!error)
          {
              //NSLog(@"Successfully retrieved the user");
              //extract the file of the profile pic from the user
              PFFile *profilePicturePFFile = [user valueForKeyPath:keyProfilePicture];
              
              //get the profile pic
              [profilePicturePFFile getDataInBackgroundWithBlock:^(NSData *data, NSError *error)
              {
                  if (!error)
                  {
                      //set the imgage from the data
                      UIImage *image = [UIImage imageWithData:data];
                     
                      //set the image in the ui view
                      viewForImage.image=image;
                      
                  }
    
              }];
              
          }
         
     }];
}


-(STRPost*)getPostFromDictionary:(NSDictionary*)dictionary
{
    //send dictionary get post
    STRPost* post = [[STRPost alloc]init];
    post.activityType = dictionary[KeyActivityType];
    post.activityTime = dictionary[KeyActivityTime];
    post.activityDesc = dictionary[KeyActivityDesc];
    post.workoutLevel = dictionary[KeyWorkoutLevel];
    
    
    post.user = dictionary[@"User"];
    
    post.activityPoint = dictionary[KeyActivityLocation];
    
    return post;
    
}

@end