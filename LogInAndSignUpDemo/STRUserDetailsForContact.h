//
//  STRUserDetailsForContact.h
//  Sporty
//
//  Created by gonzo bonzo on 4/13/14.
//
//

#import <UIKit/UIKit.h>

@interface STRUserDetailsForContact : UIViewController
//outlets
@property (weak, nonatomic) IBOutlet UIButton *imgUserImage;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UILabel *lblUserPhone;
@property (weak, nonatomic) IBOutlet UIButton *btnMaleFemaleCircle;
@property (weak, nonatomic) IBOutlet UIButton *btnContactBySMS;
@property (weak, nonatomic) IBOutlet UIButton *btnCallUser;
//properties
@property (strong, nonatomic) PFUser *userDetails;



//Actions
- (IBAction)btnCallUserTapped:(UIButton *)sender;
- (IBAction)btnAnonymousCallTapped:(UIButton *)sender;
- (IBAction)btnSendSMSTapped:(UIButton *)sender;
@end
