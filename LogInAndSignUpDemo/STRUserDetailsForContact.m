//
//  STRUserDetailsForContact.m
//  Sporty
//
//  Created by gonzo bonzo on 4/13/14.
//
//

#import "STRUserDetailsForContact.h"

@interface STRUserDetailsForContact ()
@property (strong,nonatomic) PFObject* answerByUserDetails;
@end

@implementation STRUserDetailsForContact

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    [self getAnsweredByUserDetails];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getAnsweredByUserDetails
{
    // get User details
    self.answerByUserDetails = [PFQuery getUserObjectWithId:self.userDetails.objectId];
    
    if (self.answerByUserDetails)
    {
        [self setUserDetails];
    }
    
}

-(void)setUserDetails
{
    //self.userDetails
    
    
    //set the user details in the labels
    //set the user name
    NSDictionary* userData = [NSDictionary dictionaryWithDictionary:[self.answerByUserDetails valueForKey:KeyEstimatedData]];
    self.lblUserName.text= [userData valueForKey:@"username"];
    
    //set the phone number in label
    self.lblUserPhone.text = [userData valueForKey:KeyPhoneNumber];
    
    //set circale button on male or female
    //get the gender from user class
    NSString *gender =[userData valueForKey:keyGender];
    
    if([gender isEqualToString:keyGenderFemale])
    {
        //if the string is a female- set the button circle on the female picture
        self.btnMaleFemaleCircle.frame=CGRectMake(101, 220, 50, 50);
    
    }
    else
    {
        //if the string is a male- set the button circle on the male picture
        self.btnMaleFemaleCircle.frame=CGRectMake(179, 220, 50, 50);
    }
    
    //set the buttons labels
    [self.btnContactBySMS setTitle:[NSString stringWithFormat:@"Contact %@ by SMS", self.lblUserName.text] forState:UIControlStateNormal];
    [self.btnCallUser setTitle:[NSString stringWithFormat:@"Call %@", self.lblUserName.text] forState:UIControlStateNormal];
    
}

#pragma mark - contact buttons

- (IBAction)btnCallUserTapped:(UIButton *)sender
{
    //get the phone number
    NSString* tel = [NSString stringWithFormat:@"telprompt://%@", self.lblUserPhone.text];
    
    [self dialPhoneNumberWithString:tel];
}
- (IBAction)btnAnonymousCallTapped:(UIButton *)sender
{
    
    //get the number and add the *43
    NSString* tel = [NSString stringWithFormat:@"telprompt://*43%@", self.lblUserPhone.text];

    [self dialPhoneNumberWithString:tel];
}

- (IBAction)btnSendSMSTapped:(UIButton *)sender
{
    //get the phone number
    NSString* sms = [NSString stringWithFormat:@"sms:%@", self.lblUserPhone.text];

    
    //Check if the device can "send sms"
    UIDevice *device = [UIDevice currentDevice];
    if ([[device model] isEqualToString:@"iPhone"] ) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:sms]];
    }
    else //device cannot make calls, Issue alert
    {
        NSString* messeage = [NSString stringWithFormat:@"  שוב אתה משתמש בסימולטור..? אגב, אם בא לך בכל זאת לדבר עם %@, אז אולי פשוט תתקין על הטלפון שלך את האפליקציה..? \n מה אתה עושה פרצוף ?\nמה אתה כאילו מתעצל עכשיו ?",self.lblUserName.text];
        UIAlertView *Notpermitted=[[UIAlertView alloc] initWithTitle:@"בעיה" message:messeage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [Notpermitted show];
        
    }

    
}
-(void)dialPhoneNumberWithString:(NSString*)phone
{
    //Check if the device can "make calls"
    UIDevice *device = [UIDevice currentDevice];
    if ([[device model] isEqualToString:@"iPhone"] ) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phone]];
    }
    else //device cannot make calls, Issue alert
    {
        NSString* messeage = [NSString stringWithFormat:@"  שוב אתה משתמש בסימולטור..? אגב, אם בא לך בכל זאת לדבר עם %@, אז אולי פשוט תתקין על הטלפון שלך את האפליקציה..? \n מה אתה עושה פרצוף ?\nמה אתה כאילו מתעצל עכשיו ?",self.lblUserName.text];
        UIAlertView *Notpermitted=[[UIAlertView alloc] initWithTitle:@"בעיה" message:messeage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [Notpermitted show];
        
    }

}
@end
