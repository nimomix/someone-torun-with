//
//  STRShowLocationVC.m
//  Sporty
//
//  Created by gonzo bonzo on 8/24/14.
//
//

#import "STRShowLocationVC.h"

@interface STRShowLocationVC ()

@property (strong,nonatomic) CLLocationManager *loctionManger;
@property (strong,nonatomic) CLGeocoder *geocoder;

@property (strong,nonatomic)  MapLocation *currentPin;

@end

@implementation STRShowLocationVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //set map on current possion
    if(self.currentLocation!=nil)
    {
        //recantere map on it
        [self recenterMap:self.currentLocation];
        
        //make an a pin on this location with the street details as subtitle
        [self createPinOnMapWithStreetDetails:self.currentLocation];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void) performSearch
{
    //create a search request
    MKLocalSearchRequest *request = [[MKLocalSearchRequest alloc] init];
    //get text from search field - set it ti request
    //request.naturalLanguageQuery = self.serachBar.text;
    //NSLog(@"Searching: %@",self.serachBar.text);
    request.region = self.mapView.region;
    
    self.matchingItems = [[NSMutableArray alloc] init];
    
    MKLocalSearch *search = [[MKLocalSearch alloc]initWithRequest:request];
    
    [search startWithCompletionHandler:^(MKLocalSearchResponse
                                         *response, NSError *error) {
        if (response.mapItems.count == 0)
            NSLog(@"No Matches");
        else
            for (MKMapItem *item in response.mapItems)
            {
                [self.matchingItems addObject:item];
                self.currentPin = [[MapLocation alloc]init];
                self.currentPin.coordinate = item.placemark.coordinate;
                //take all details from placemark and put it on pin
                self.currentPin.street = item.name;
                //                self.currentPin.city = item.placemark;
                
                //self.currentPin.title = item.name;
                [self.mapView addAnnotation:self.currentPin];
                
                CLLocation *location = [[CLLocation alloc]initWithLatitude:item.placemark.coordinate.latitude longitude:item.placemark.coordinate.longitude];
                
                
                [self recenterMap:location];
                
                NSLog(@"%lu Matches!",(unsigned long)response.mapItems.count);
            }
    }];
}

-(void) recenterMap: (CLLocation*) location
{
    
    //create a region around the location in size...
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, 200, 200);
    //fit this shape in map view
    MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:viewRegion];
    //set the map region to this new region
    [self.mapView setRegion:adjustedRegion animated:YES];
    
}

-(void) reverseGeocodeLocation: (CLLocation*)location
{
    
    //create a geo coder
    CLGeocoder *geocoder=[[CLGeocoder alloc]init];
    
    
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error)
     {
         if(error ==nil)
         {
             if(placemarks.count>0)
             {
                 CLPlacemark *placemark = [placemarks objectAtIndex:0];
                 
                 self.currentPin = [[MapLocation alloc]init];
                 self.currentPin.street = placemark.thoroughfare;
                 self.currentPin.city = placemark.locality;
                 self.currentPin.state = placemark.administrativeArea;
                 self.currentPin.zip = placemark.postalCode;
                 self.currentPin.coordinate = location.coordinate;
                 
                 //remove all other annotations before adding one
                 [self.mapView removeAnnotations:[self.mapView annotations]];
                 
                 //add the new annotation
                 [self.mapView addAnnotation:self.currentPin];
                 
                 //create a text from the valus
                 //NSString *locationInText = [NSString stringWithFormat:@"%@, %@ ",placemark.thoroughfare ,placemark.locality];
                 
                 //set the location label
                 //self.lblActivityLocation.text=locationInText;
                 
                 //stop getting updates on location
                 [self.loctionManger stopUpdatingLocation];
                 self.loctionManger =nil;
                 
                 
                 
             }
             
             else
             {
                 //if we dont get a placemark array we will show alert daialog
                 UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error translating coordinates" message:@"goecoder did not recognize coordinates" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                 
                 [alert show];
                 
             }
         }
     }
     ];
    
}

-(void) createPinOnMapWithStreetDetails: (CLLocation*)location
{
    
    
    if(!self.geocoder)
    {
        self.geocoder=[[CLGeocoder alloc]init];
        
        [self.geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error)
         {
             if(error ==nil)
             {
                 if(placemarks.count>0)
                 {
                     CLPlacemark *placemark = [placemarks objectAtIndex:0];
                     
                     //want to see what we have in placemark? anable this 2 lines
                     //                    NSDictionary *addressDictionary = placemark.addressDictionary;
                     //                    NSLog(@"%@ ", addressDictionary);
                     
                     
                     
                     
                     //create a pin
                     self.currentPin = [[MapLocation alloc]init];
                     //take all details from placemark and put it on pin
                     self.currentPin.street = placemark.name;
                     self.currentPin.city = placemark.locality;
                     //since we are in israel we won't use "state"
                     //annotation.state = placemark.administrativeArea;
                     self.currentPin.zip = placemark.postalCode;
                     self.currentPin.coordinate = location.coordinate;
                     
                     
                     
                     
                     //add pin to map
                     [self.mapView addAnnotation:self.currentPin];
                     
                     
                     
                     //set geocoder to nil for next task
                     // self.geocoder=nil;
                     
                 }
                 
                 else
                 {
                     //if we dont get a placemark array we will show alert daialog
                     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error translating coordinates" message:@"goecoder did not recognize coordinates" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                     
                     [alert show];
                     
                 }
             }
         }];
    }
}


@end
