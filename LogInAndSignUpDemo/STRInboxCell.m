//
//  STRInboxCell.m
//  Sporty
//
//  Created by נמרוד בר on 18/11/13.
//
//

#import "STRInboxCell.h"

@implementation STRInboxCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)SetCurrentPostInCell:(NSDictionary*)post
{

    NSString *userName = [post valueForKey:KeyUserName];
    self.lblUserName.text = userName;
    
    
    
}
@end
