//
//  STRConstants.m
//  Sporty
//
//  Created by Ariel Peremen on 10/25/13.
//
//

///username objectId activityTime activityType activityDesc serverData Phone

#import "STRConstants.h"

@implementation STRConstants : NSObject 

NSString *const MyThingNotificationKey = @"MyThingNotificationKey";

//app key words
NSString *const KeyUserName= @"userName";
NSString *const KeyActivityTime= @"activityTime";
NSString *const KeyActivityType= @"activityType";
NSString *const KeyActivityDesc= @"activityDesc";
NSString *const KeyPhoneNumber=@"Phone";
NSString *const KeyWorkoutLevel=@"workoutLevel";
NSString *const KeyActivityLocation=@"location";

//server keys
NSString *const KeyUser= @"user";
NSString *const KeyObjectId= @"objectId";
NSString *const KeyCreatedAt= @"createdAt";
NSString *const KeyClassUser= @"User";
NSString *const KeyClassPosts= @"Posts";
NSString *const KeyClassAnswers= @"Answers";
NSString *const keyPost= @"Post";
NSString *const keyAnsweredBy= @"AnsweredBy";
NSString *const keyGender= @"Gender";
NSString *const keyGenderFemale= @"female";
NSString *const keyGenderMale= @"male";
NSString *const keyProfilePicture= @"ProfilePicture";



NSString *const KeyEstimatedData= @"estimatedData";
NSString *const KeyServerData= @"serverData";
//NSString *const KeyClassAnswers= @"Answers";


//app parameters
int const RadiosForPostSearch=50000000;

//languges
NSString *const KeyMenuHome=@"פעילות";
NSString *const KeymenuMessegesBox= @"תיבת הודעות";
NSString *const KeyMenuUserDetails=@"פרטים אישיים";
NSString *const KeyMenuRunning=@"ריצה";
NSString *const KeyMenuSwimming=@"שחייה";
NSString *const KeyMenuGim=@"חדר כושר";
NSString *const KeyMenuWalking=@"הליכה";
NSString *const KeyMenuTennis=@"טניס";
NSString *const KeyMenuBasketball=@"כדורסל";
NSString *const KeyMenuSoccer=@"כדורגל";
NSString *const KeyMenuBicycle=@"אופניים";
NSString *const KeyMenuSpiritual=@"רוחני";
NSString *const KeyMenuOther=@"אחר";


NSString *const KeyRunning=@"running";
NSString *const KeySwimming=@"swimming";
NSString *const KeyGim=@"gim";
NSString *const KeyWalking=@"walking";
NSString *const KeyTennis=@"tennis";
NSString *const KeyBasketball=@"basketball";
NSString *const KeySoccer=@"soccer";
NSString *const KeyBicycle=@"bicycle";
NSString *const KeySpiritual=@"spiritual";
NSString *const KeyOther=@"other";



NSString *const KeyMenuGroups=@"קבוצות";
NSString *const KeyMenuAboutUs=@"עלינו";
NSString *const KeyMenuReport=@"דווח";
NSString *const KeyMenuLogOut=@"התנתק";

//Distance from current post


NSString *const KeyDistanceMoreThenHour=@"יותר משעה נסיעה";
NSString *const KeyDistanceBicyle=@"דקות באופניים";
NSString *const KeyDistanceDriving=@"דקות נסיעה";
NSString *const KeyDistanceWalking=@"דקות הליכה";
NSString *const KeyDistanceHere=@"ממש פה ליד";

//When labels are empty

NSString *const KeyNoDetails=@"ספורט באופן כללי";
NSString *const KeyNoPartnersCell=@"כולם פדלאות? ליצירת פעילות ";
NSString *const KeyAlertTitleMissingDetails=@"היי לאט לאט";
NSString *const KeyAlertMissingActivityLocation=@"שכחת למלא איפה האימון יהיה";
NSString *const KeyAlertMissingActivityDate=@"שכחת למלא את זמן הפעילות שלך";
NSString *const KeyAlertMissingActivityType=@"שכחת למלא את סוג הפעילות שלך";
NSString *const KeyAlertMissingActivityDetailsOkButton=@"הא נכון, תנו לי רגע לתקן את זה";
NSString *const KeyWhen=@"מתי מתחילים?";



@end
