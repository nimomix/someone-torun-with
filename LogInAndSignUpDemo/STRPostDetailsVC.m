//
//  STRPostDetailsVC.m
//  Sporty
//
//  Created by נמרוד בר on 31/10/13.
//
//

#import "STRPostDetailsVC.h"

@interface STRPostDetailsVC () <UIAlertViewDelegate, PFLogInViewControllerDelegate, PFSignUpViewControllerDelegate>

@property (strong,nonatomic)PFUser* postUser;
@property (strong,nonatomic) CLGeocoder *geocoder;

@end



@implementation STRPostDetailsVC



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        //set current answers to post to nil
        self.answeredPostObjectId = nil;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setCurrentPostLabels];
    
    // Create thread to run in the background query for getting post user
    NSThread *thread=[[NSThread alloc]initWithTarget:self selector:@selector(getUserDetails) object:nil];
    [thread start];
    
    //set this calss to be the map delegate
    //self.mapView.delegate=self;
    
    [self setScreenBackgroundColor];
    
    //set contact button to contact user or cancel the contact
    [self setTypeOfContactUserButton];
    
    [self setLabelsProperties];
}

-(void)setScreenBackgroundColor
{
    self.view.backgroundColor=COLORBackgroundWhite;
}

-(void) setLabelsProperties
{
    self.lblActivityType.textColor=COLORRed;
    self.lblActivityDesc.textColor=COLORDarkBlue;
    self.lblActivityLocation.textColor=COLORDarkBlue;
    self.lblActivityTime.textColor=COLORDarkBlue;
    self.lblUserName.textColor=COLORDarkBlue;
    
    //set the labels font
    self.lblUserName.font=FONTAlefRegular14;
    self.lblActivityTime.font=FONTAlefBold17;
    self.lblActivityType.font=FONTAlefBold50;
    self.lblActivityDesc.font=FONTAlefBold17;
    self.lblActivityLocation.font=FONTAlefBold17;
    
    //static words
    self.lblEasy.font=FONTAlefRegular14;
    self.lblMedium.font=FONTAlefRegular14;
    self.lblHard.font=FONTAlefRegular14;
    
    //set title
    self.title=@"פרטי הפעילות";
   

    

}

-(void)setTypeOfContactUserButton
{
    if (self.answeredPostObjectId!=nil)
    {
        //the current user already answerd this post- we will change button to cancal contact
        [self.btnContact setTitle:@"בטל התקשרות" forState:UIControlStateNormal];
                [self.btnContact removeTarget:self action:@selector(contactUserTapped) forControlEvents:UIControlEventTouchUpInside];
        [self.btnContact addTarget:self action:@selector(deleteCurrentUserAnswerTapped) forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
        //the user never contact this post
        [self.btnContact setTitle:@"צור קשר" forState:UIControlStateNormal];
        [self.btnContact removeTarget:self action:@selector(deleteCurrentUserAnswerTapped) forControlEvents:UIControlEventTouchUpInside];
        [self.btnContact addTarget:self action:@selector(contactUserTapped) forControlEvents:UIControlEventTouchUpInside];

    }
    
}
-(void)deleteCurrentUserAnswerTapped
{
     
    
    //Create query to find Answer to delete
    PFQuery *answerQuery = [PFQuery queryWithClassName:KeyClassAnswers];
    
    //Filter by currentPost
    [answerQuery whereKey:keyPost equalTo:self.PFObjectCurrentPost];
    
    //Filter by current user
    [answerQuery whereKey:keyAnsweredBy equalTo:[PFUser currentUser]];
    
    //Run query
    [answerQuery getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        
        //Delete the object
        [object deleteInBackground];
    }];
    
    for (int i = 0; i < [AppData sharedInstance].postsAnswered.count; i++) {
        if ([[AppData sharedInstance].postsAnswered[i] isEqualToString:self.PFObjectCurrentPost.objectId]) {
            [[AppData sharedInstance].postsAnswered removeObjectAtIndex:i];
            [[AppData sharedInstance]saveDataToDisk];
     
        }
    }
    self.answeredPostObjectId = nil;
    [self setTypeOfContactUserButton];

}

- (void)contactUserTapped
{
    if (![PFUser currentUser])
    {
        //who to call the login screen
        
        // Create the log in view controller
        PFLogInViewController *logInViewController = [[PFLogInViewController alloc] init];
        [logInViewController setDelegate:self]; // Set ourselves as the delegate
        
        // Create the sign up view controller
        PFSignUpViewController *signUpViewController = [[PFSignUpViewController alloc] init];
        [signUpViewController setDelegate:self]; // Set ourselves as the delegate
        
        // Assign our sign up controller to be displayed from the login controller
        [logInViewController setSignUpController:signUpViewController];
        
        // Present the log in view controller
        [self presentViewController:signUpViewController animated:YES completion:NULL];
        
    }
    else
    {
        [self contactUser];
        
        
    }
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//Run this function only in thread
-(void)getUserDetails
{
    
    //Because user is complicated object inside the currentPost, we use double getting
    PFObject* userObj = [self.currentPost valueForKey:KeyClassUser];
    NSString *userId = [userObj valueForKey:KeyObjectId];
    
    //postUser is Global variable of PFUser
    self.postUser = [PFQuery getUserObjectWithId:userId];
    
    // Put the data to the labels only after the thread is done (we are inside thread)
    //[self setCurrentUserDetails];
    
    
    
}

-(void) setCurrentPostLabels
{


    
    
    [self setWorkoutSliderProperties];
    
    
    //set the user name labal
    self.lblUserName.text = [self.currentPost valueForKey:KeyUserName];
    //get the create date of the post
    NSString* date = [[AppData sharedInstance] dateOrgenizer:[self.currentPost valueForKey:KeyCreatedAt]];
    //set the post creation date labal
    self.lblCreatedAt.text = date;
    
    
    //get the activity time
    NSString* time = [[AppData sharedInstance] dateOrgenizer:[self.currentPost valueForKey:KeyActivityTime]];
    //set the activity time label
    self.lblActivityTime.text = time;
    
    
    
    //set the activity descreption
    [self setActivityDescreption];
    
    //set activity type details
    [self setActivityTypeProperties];
    
        //get the PFGeoPoint of the activity
    PFGeoPoint *pfgeoPointLocation =[self.currentPost valueForKey:KeyActivityLocation];
    //create location based on geo point from latitude and longitude
    CLLocation *activityLocation=[[CLLocation alloc]initWithLatitude:pfgeoPointLocation.latitude longitude:pfgeoPointLocation.longitude];
    
    //set location label
    [self setLocationLabel:activityLocation];
    
    //recantere map on the location
    //[self recenterMap:activityLocation];
    
    //set a pin on the location
    //[self createPinOnMapWithStreetDetails:activityLocation];
    //set the user profile pic
    [self setUserProfilePicture];
    //set the user gender pic
    [self setUserGenderInPicture];
    
    //set the workoutlevel
    [self setWorkoutLevel];
    
}

-(void) setActivityDescreption
{
    //set avtivity descreption
    NSString *descreption = [self.currentPost valueForKey:KeyActivityDesc];
    if([descreption isEqualToString:@""])
    {
        descreption= KeyNoDetails;
    }
    self.lblActivityDesc.text=descreption;
    

}

-(void) setActivityTypeProperties
{
    //get activity type name
    NSString *activityTypeName = [self.currentPost valueForKey:KeyActivityType];
    //convert it to hebrew
    NSString *activityTypeNameHebrew = [[AppData sharedInstance]convertActivityTypeToHebrew:activityTypeName];
   
    
    //create attributs for text
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    //set the space hight between words
    paragraphStyle.maximumLineHeight = 40.f;
    //makethe text alighn to right
    paragraphStyle.alignment=NSTextAlignmentRight;
    //put the pargraph properties into array
    NSDictionary *attributtes = @{NSParagraphStyleAttributeName : paragraphStyle,};
    //create the atrributed string
    NSAttributedString *activityNameAttributed=[[NSAttributedString alloc]initWithString:activityTypeNameHebrew attributes:attributtes];
    //set the attributed text in the label
    self.lblActivityType.attributedText  =activityNameAttributed;
    

    
    //set the image of the activty based on the name
    UIImage *activityTypeImage;
    
    if([activityTypeName isEqualToString:KeyBasketball])
    {
        //if the activity type is basketball convert it to hebrew
        activityTypeImage=[UIImage imageNamed:@"basketball_a"];
    }
    else if ([activityTypeName isEqualToString:KeyRunning])
    {
        activityTypeImage=[UIImage imageNamed:@"running_a"];
    }
    else if ([activityTypeName isEqualToString:KeySwimming])
    {
        activityTypeImage=[UIImage imageNamed:@"swimming_a"];
    }
    else if ([activityTypeName isEqualToString:KeyGim])
    {
        activityTypeImage=[UIImage imageNamed:@"gym_a"];
    }
    else if ([activityTypeName isEqualToString:KeyWalking])
    {
        activityTypeImage=[UIImage imageNamed:@"basketball_a"];
    }
    else if ([activityTypeName isEqualToString:KeyTennis])
    {
        activityTypeImage=[UIImage imageNamed:@"tennis_a"];
    }
    else if ([activityTypeName isEqualToString:KeySoccer])
    {
        activityTypeImage=[UIImage imageNamed:@"soccer_a"];
    }
    else if ([activityTypeName isEqualToString:KeyBicycle])
    {
        activityTypeImage=[UIImage imageNamed:@"bicycle_a"];
    }
    else if ([activityTypeName isEqualToString:KeySpiritual])
    {
        activityTypeImage=[UIImage imageNamed:@"spiritual_a"];
    }
    else if ([activityTypeName isEqualToString:KeyOther])
    {
        activityTypeImage=[UIImage imageNamed:@"menuIcon.png"];
    }


    //set the activity type image
    self.imgActivityType.image=activityTypeImage;
    
    
    
}

-(void) setWorkoutSliderProperties
{

    //set min/max valus
    self.sliderWorkoutLevel.maximumValue=3;
    self.sliderWorkoutLevel.minimumValue=1;
    //don't let user chage it
    self.sliderWorkoutLevel.userInteractionEnabled=NO;
    //set the color of the line
    self.sliderWorkoutLevel.tintColor=COLORRed;
    //set the dot on the slider
    [self.sliderWorkoutLevel setThumbImage:[UIImage imageNamed:@"level_button-Smaller.png"] forState:UIControlStateNormal];
}

-(void) setWorkoutLevel
{
    //create int for the workout level
    int workoutLevel=0;
    //get the workot level from the post
    workoutLevel= [[self.currentPost valueForKey:KeyWorkoutLevel] intValue];
    //if the value changed from 0
    if(workoutLevel!=0)
    {
        //set it to the slider
        self.sliderWorkoutLevel.value=workoutLevel;
        
    }
    
    
}

-(void)setUserGenderInPicture
{
    //get the user gender to string
    NSString *gender =[self.currentPost valueForKey:keyGender];

    if ([gender isEqualToString:keyGenderMale])
    {
        //set the pic to male
        self.imgUserGender.image=[UIImage imageNamed:@"maleIcon.png"];
    }
    else if ([gender isEqualToString:keyGenderFemale])
    {
        //set the pic to female
        self.imgUserGender.image=[UIImage imageNamed:@"femaleIcon.png"];
    }
    else
    {
        //if we go to here it means the post don't know what is the user gender
        NSLog(@"post dosn't have user's gender");
        //set the pic to none
        self.imgUserGender.image=nil;   
    }
    
}

-(void) setUserProfilePicture
{
    //get the user that created the post
    PFUser *user = [self.currentPost valueForKey:KeyClassUser];

    //set the user profile pic
    [[AppData sharedInstance] getProfileImageFromServer:user.objectId andUiImage:self.imgProfilePicture];
    
}

-(void)setLocationLabel:(CLLocation*) location
{
    
//take location - convert it to string and set it in label
    [self reverseGeocodeToLocationLabel: location];
    
    
}

-(void) reverseGeocodeToLocationLabel: (CLLocation*)location
{
    if(!self.geocoder)
    {
        self.geocoder=[[CLGeocoder alloc]init];
        
        [self.geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error)
         {
             if(error ==nil)
             {
                 if(placemarks.count>0)
                 {
                     CLPlacemark *placemark = [placemarks objectAtIndex:0];
                     
                     MapLocation *annotation = [[MapLocation alloc]init];
                     annotation.street = placemark.thoroughfare;
                     annotation.subThoroughfare= placemark.subThoroughfare;
                     annotation.city = placemark.locality;
                     annotation.state = placemark.administrativeArea;
                     annotation.zip = placemark.postalCode;
                     annotation.coordinate = location.coordinate;
                     
                     
                    // [self.mapView addAnnotation:annotation];
                     
                     //create a text from the valus
                     NSString *locationInText;
                     //if we don't have a value in subThoroughfare (street number) we won't write it
                     if(placemark.subThoroughfare==NULL)
                     {
                         locationInText = [NSString stringWithFormat:@"%@, %@ ",placemark.thoroughfare ,placemark.locality];
                     }
                     else
                     {
                         locationInText = [NSString stringWithFormat:@"%@ %@, %@ ",placemark.thoroughfare, placemark.subThoroughfare ,placemark.locality];
                     }
                     
                     
                     
                     //set the location label
                     self.lblActivityLocation.text=locationInText;
                     
                     //set geocoder to nil for next task
                     self.geocoder=nil;
                     
                 }
                 
                 else
                 {
                     //if we dont get a placemark array we will show alert daialog
                     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error translating coordinates" message:@"goecoder did not recognize coordinates" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                     
                     [alert show];
                     
                 }
             }
         }];
    }
}


//-(void) recenterMap: (CLLocation*) location
//{
//    
//    
//    //create a region around the location in size...
//    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, 500, 500);
//    //fit this shape in map view
//    MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:viewRegion];
//    //set the map region to this new region
//    [self.mapView setRegion:adjustedRegion animated:YES];
//    
//}

//-(void) createPinOnMapWithStreetDetails: (CLLocation*)location
//{
//    //create a geo coder
//    CLGeocoder *geocoder=[[CLGeocoder alloc]init];
//    //get array of placemarks based on location
//    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error)
//     {
//         if(error ==nil)
//         {
//             if(placemarks.count>0)
//             {
//                 //get the first name from the list
//                 CLPlacemark *placemark = [placemarks objectAtIndex:0];
//                 
//                 
//                 //create a pin
//                 MapLocation *annotation = [[MapLocation alloc]init];
//                 //take all details from placemark and put it on pin
//                 annotation.street = placemark.name;
//                 annotation.city = placemark.locality;
//                 //since we are in israel we won't use "state"
//                 //annotation.state = placemark.administrativeArea;
//                 annotation.zip = placemark.postalCode;
//                 annotation.coordinate = location.coordinate;
//                 
//                 //add pin to map
//                 [self.mapView addAnnotation:annotation];
//                 
//             }
//             
//             else
//             {
//                 //if we dont get a placemark array we will show alert daialog
//                 UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error translating coordinates" message:@"goecoder did not recognize coordinates" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
//                 
//                 [alert show];
//                 
//             }
//         }
//     }];
//}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //we can get here in three states
    //1. with phone that user already have
    //2. phone in text input
    //3. empty text input - no phone

    //-----TESTING ONLY------
    //erase phone number
//    PFUser *user = [PFUser currentUser];
//    user[KeyPhoneNumber] = @"";
//    [user saveInBackground];
//PFUser *user = [PFUser currentUser];
//    [user refresh];
    
    
    //user didn't canceld - sending with phone
    if (buttonIndex != alertView.cancelButtonIndex)
    {
        //Checks if there is a text input (to update phone number)
        if(alertView.alertViewStyle==UIAlertViewStylePlainTextInput)
        {
            //convert input to string
            NSString *textFieldInput =[[alertView textFieldAtIndex:0] text];
            
            //check that the input is a valied phone number
            if ([self isValidPhoneNumber:textFieldInput])
            {
                //update the current user phone from input textfield in his details
                PFUser *currentUser = [PFUser currentUser];
                currentUser[KeyPhoneNumber] = textFieldInput;
                
                //saves and create an "Answer class" row once completed
                [currentUser saveInBackgroundWithTarget:self selector:@selector(postAnswerInBackground)];
                
            }
            else
            {
                [self NoNumberInputAlert];
            }
            
        }
        else
        {
            //if the current user already has a phone updated.- create an "Answer class" row

            [self postAnswerInBackground];
        
        }
        
    }
    else
    {
        // this is where you would handle any actions for "Cancel"
        NSLog(@"no massage has been sent- you canceld");
    }
}
-(void)NoNumberInputAlert
{
    //ask for the user phone number
    UIAlertView* getPhoneNumberAlert = [[UIAlertView alloc]initWithTitle:@"No phone" message:@"please enter your phone number" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"send", nil];
    
    //set this alert view to be with text input
    getPhoneNumberAlert.alertViewStyle=UIAlertViewStylePlainTextInput;
    
    //show alert view
    [getPhoneNumberAlert show];
}
-(void)postAnswerInBackground
{
    [self postAnswer];
    [[AppData sharedInstance].postsAnswered addObject:[self.currentPost valueForKey:KeyObjectId]];
    [[AppData sharedInstance] saveDataToDisk];

}


-(BOOL)isValidPhoneNumber:(NSString*)textFieldInput
{
    NSString* prenumber = [textFieldInput substringToIndex:2];
    if ([prenumber isEqualToString:@"05"])
    {
        NSCharacterSet* doNotWant = [NSCharacterSet characterSetWithCharactersInString:@"-=/*+_ .:#$%^&*()!@`;"];
        NSString* phone = [[textFieldInput componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString: @""];
        if (phone.length == 10)
        {
            return YES;
        }
    }
    else
    {
        return NO;
    }
    
    return NO;
    
}



-(void)postAnswer
{
    
    //get current user
    PFUser *currentUser=[PFUser currentUser];
    
    //Create PFobject reply
    PFObject* reply = [[PFObject alloc]initWithClassName:KeyClassAnswers];
    
    //set the current user pointer to anserd by collom
    [reply setObject:currentUser forKey:@"AnsweredBy"];

    //set the post owner pointer
    [reply setObject:[self.currentPost valueForKey:KeyClassUser]forKey:@"PostBy"];
    
    //set current post
    [reply setObject:self.PFObjectCurrentPost forKey:@"Post"];
    
    //set the activity type and time
    [reply setObject:[self.currentPost valueForKey:KeyActivityType] forKey:KeyActivityType];
    [reply setObject:[self.currentPost valueForKey:KeyActivityTime] forKey:KeyActivityTime];
     
    //set the answerd by username to the post
    [reply setObject:currentUser.username forKey:KeyUserName];
    
    
    
    
          //Commit to DB
        [reply saveInBackgroundWithTarget:self selector:@selector(saveAnswerInBackgroundDone)];
    
    

}
- (void)getCallback:(PFObject *)savedResult error:(NSError *)error
{
    NSLog(@"%@",savedResult.objectId);
}

-(void)saveAnswerInBackgroundDone
{
    self.answeredPostObjectId = self.PFObjectCurrentPost.objectId;
    [self setTypeOfContactUserButton];
}


// Set the details only after the thread is done.
-(void) setCurrentUserDetails
{
    //NSDictionary *userServerData= [self.postUser valueForKey:KeyServerData];
    //NSString *userPhone=[userServerData valueForKey:KeyPhoneNumber];
    
    //self.lblUserPhoneNumber.text=userPhone;
}



-(void)contactUser
{
    //get current user
    PFUser *currentUser=[PFUser currentUser];
    
    //check the user phone
    NSDictionary *serverData =[currentUser valueForKey:KeyServerData];
    NSString *phone = [serverData valueForKey:KeyPhoneNumber];
    
    
    if (phone!=nil && ![phone isEqualToString:@""])
    {
        UIAlertView* privacyAlert = [[UIAlertView alloc]initWithTitle:@"Contacting" message:@"Your details including your phone-number will be sent to the Post owner for contacting" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"send", nil];

        [privacyAlert show];
    }
    else
    {
        [self NoNumberInputAlert];
    }

}
- (IBAction)btnLocationDetailsTapped:(id)sender
{
    //we will desplay the map to view the pin - of the activity location
    
    //get the PFGeoPoint of the activity
    PFGeoPoint *pfgeoPointLocation =[self.currentPost valueForKey:KeyActivityLocation];
    //create location based on geo point from latitude and longitude
    CLLocation *activityLocation=[[CLLocation alloc]initWithLatitude:pfgeoPointLocation.latitude longitude:pfgeoPointLocation.longitude];
    
    
    //create the VC which will present the post
    STRShowLocationVC* ShowLocation = [[STRShowLocationVC alloc]initWithNibName:nil bundle:nil];
    
    if(activityLocation!=nil)
    {
        ShowLocation.currentLocation=activityLocation;
        
         [self.navigationController pushViewController:ShowLocation animated:YES];
    }
    
    
    
}


/// Sent to the delegate when a PFUser is signed up.
- (void)signUpViewController:(PFSignUpViewController *)signUpController didSignUpUser:(PFUser *)user
{
    NSLog(@"we have a user %@", user);
    
    [signUpController dismissViewControllerAnimated:YES completion:nil];
    
    [self contactUser];
}

/// Sent to the delegate when the sign up attempt fails.
- (void)signUpViewController:(PFSignUpViewController *)signUpController didFailToSignUpWithError:(NSError *)error
{
    NSLog(@"FAIL TO LOGIN");
}

@end
