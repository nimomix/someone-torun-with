//
//  CellForAdsList.m
//  Sporty
//
//  Created by Ariel Peremen on 9/9/13.
//
//

#import "STRPostCell.h"

@implementation STRPostCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
//        self. = NO;
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)SetCurrentPostInCell:(NSDictionary*)post
{
    //set the activity Descreption
    [self setActivityDescreption:post];
    
    //set the activity Time
    [self setActivityTimeLabel:post];
    
    //set the activity Type label
    [self setActivityTypeLabel:post];
    
    //set the activity Type Pic
    [self setActivityTypeImage:post];
    
    
    //checks if post was already answered by user
    [self checkIfAnsweredByCurrentUser:post];
    
    //set distance label with text
    [self setDistanceLabel:post];

    //set color for cell if already answered
    [self changeColorIfPostAnswerdByCurrentUser];
    
    //set the activity user's gender
    [self setUsersGenderInImage:post];
    
    //self.lblDestanceFromActivty.text = [NSString stringWithFormat:@"%.1f", distanceFromActivity];
    
    //get post user object id
   // PFUser *user= [post valueForKey:KeyClassUser];
    
    //NSString *objectId = [PFUser currentUser].objectId;
    //NSString *gender = [user valueForKey:keyGender];
    //NSLog(@"%@",gender);
//    //set the user profile picture in the profile picture imageview
//    self.imgUserProfile.contentMode =UIViewContentModeScaleAspectFill;
//    self.imgUserProfile.layer.cornerRadius=40.0f;
//    self.imgUserProfile.clipsToBounds=YES;
//    
//    [[AppData sharedInstance]getProfileImageFromServer:user.objectId andUiImage:self.imgUserProfile];
    
}

-(void)setActivityTypeLabel:(NSDictionary*)post
{
    //get the activity name
    NSString *activityTypeName =[post valueForKey:KeyActivityType];
    
    //convert it to hebrew
    NSString *activityTypeInHebrew=[[AppData sharedInstance]convertActivityTypeToHebrew:activityTypeName];
    
    //set the label font
    self.lblActivityType.font=FONTAlefBold28;
    //set the label color
    self.lblActivityType.textColor=COLORRed;
    
    //set it in label
    self.lblActivityType.text = activityTypeInHebrew;
}

-(void)setActivityTypeImage:(NSDictionary*)post
{
    //get the activity name
    NSString *activityTypeName =[post valueForKey:KeyActivityType];
    
    if([activityTypeName isEqualToString:KeyBasketball])
    {
        //if the activity type is basketball - set basketball icon
        self.imgActivityType.image=[UIImage imageNamed:@"basketball_b.png"];
    }
    else if ([activityTypeName isEqualToString:KeyRunning])
    {
        self.imgActivityType.image=[UIImage imageNamed:@"running_b.png"];
    }
    else if ([activityTypeName isEqualToString:KeySwimming])
    {
        self.imgActivityType.image=[UIImage imageNamed:@"swimming_b.png"];
    }
    else if ([activityTypeName isEqualToString:KeyGim])
    {
        self.imgActivityType.image=[UIImage imageNamed:@"gym_b.png"];
    }
    else if ([activityTypeName isEqualToString:KeyWalking])
    {
        self.imgActivityType.image=[UIImage imageNamed:@"activityPlaceHolder.png"];
    }
    else if ([activityTypeName isEqualToString:KeyTennis])
    {
        self.imgActivityType.image=[UIImage imageNamed:@"tennis_b.png"];
    }
    else if ([activityTypeName isEqualToString:KeySoccer])
    {
        self.imgActivityType.image=[UIImage imageNamed:@"soccer_b.png"];
    }
    else if ([activityTypeName isEqualToString:KeyBicycle])
    {
        self.imgActivityType.image=[UIImage imageNamed:@"bicycle_b.png"];
    }
    else if ([activityTypeName isEqualToString:KeySpiritual])
    {
        self.imgActivityType.image=[UIImage imageNamed:@"spiritual_b.png"];
    }
    else if ([activityTypeName isEqualToString:KeyOther])
    {
        self.imgActivityType.image=[UIImage imageNamed:@"activityPlaceHolder.png"];
    }
    
}

-(void)setActivityTimeLabel:(NSDictionary*)post
{
    //conert date to relative string
    NSString *activityTime=[[AppData sharedInstance] convertDateToRelativeTime:[post valueForKey:KeyActivityTime]];
    
    //set the label font
    self.lblActivityTime.font=FONTAlefRegular14;
    //set the label color
    self.lblActivityTime.textColor=COLORDarkBlue;
    //set the text in the label
    self.lblActivityTime.text = activityTime;
}

-(void) changeColorIfPostAnswerdByCurrentUser
{
    if (self.answerObjectId!=nil)
    {
        //self.backgroundColor = [UIColor colorWithRed:0 green:0.188235 blue:0.313725 alpha:0.2];
        self.imgContactedIndecator.image=[UIImage imageNamed:@"small_red_tab"];
    }
    else self.imgContactedIndecator.image=nil;
        //self.backgroundColor = [UIColor clearColor];
}

-(void)setActivityDescreption:(NSDictionary*)post
{
    
    //set the label font
    self.lblActivityDesc.font=FONTAlefRegular20;
    //set the label color
    self.lblActivityDesc.textColor=COLORLightBlue;
    
    if ([[post valueForKey:KeyActivityDesc]isEqualToString:@""])
    {
        self.lblActivityDesc.text = KeyNoDetails;
    }
    else
    {
        ///////////string attributes//////////
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        //paragraphStyle.minimumLineHeight = 15.f;
        paragraphStyle.maximumLineHeight = 18.f;
        
        //UIFont *font = [UIFont fontWithName:@"AmericanTypewriter" size:18.f];
        
        //NSDictionary *attributtes = @{ NSParagraphStyleAttributeName : paragraphStyle,NSKernAttributeName : @(-1.3f)};
        NSDictionary *attributtes = @{
                                      NSParagraphStyleAttributeName : paragraphStyle,
                                      };
        //self.lblTitle.font = font;
        self.lblActivityDesc.attributedText = [[NSAttributedString alloc] initWithString:[post valueForKey:KeyActivityDesc] attributes:attributtes];
        
        //alighn text to right
        self.lblActivityDesc.textAlignment=NSTextAlignmentRight;
        //add 3 dots if text is long
        self.lblActivityDesc.lineBreakMode=NSLineBreakByTruncatingTail;
        
        //set text headline align to top
       // [self.lblActivityDesc sizeToFit];
        

    }
}

-(void) checkIfAnsweredByCurrentUser:(NSDictionary*)post
{
   
    if ([[AppData sharedInstance].postsAnswered containsObject:[post valueForKey:KeyObjectId]])
    {
        //NSLog(@"in cell: %lu",(unsigned long)[AppData sharedInstance].postsAnswered.count);
        //self.isAnsweredByCurrentUSer = YES;
        
        //get the current post object id
        self.answerObjectId=[post valueForKey:KeyObjectId];
        //NSLog(@"1st: %@", self.answerObjectId);
                
    }
    else
    {
    
        self.answerObjectId=nil;
    }

}

-(void) setDistanceLabel:(NSDictionary*) post
{
    //calculate distance from activity
    CLLocation* currentLoction = [[AppData sharedInstance] getCurrentLocation];
    PFGeoPoint* postGeoPoint = [post valueForKey:KeyActivityLocation];
    CLLocation* postLocation = [[CLLocation alloc]initWithLatitude:postGeoPoint.latitude longitude:postGeoPoint.longitude];
    
    
    CLLocationDistance distanceFromActivity = [currentLoction distanceFromLocation:postLocation];
    NSString *distanceInText= [self convertDistanceToText:&distanceFromActivity];
    
    
    //set the label font
    self.lblDestanceFromActivty.font=FONTAlefRegular14;
    //set the label color
    self.lblDestanceFromActivty.textColor=COLORDarkBlue;
    //set the text in the label
    self.lblDestanceFromActivty.text=distanceInText;
}


-(void) setUsersGenderInImage:(NSDictionary*) post
{
    if ([[post valueForKey:keyGender]isEqualToString:keyGenderMale])
    {
        //set the pic to male
        self.imgActivityUserGender.image=[UIImage imageNamed:@"maleIcon.png"];
        
        
    }
    else if ([[post valueForKey:keyGender]isEqualToString:keyGenderFemale])
    {
        //set the pic to female
        self.imgActivityUserGender.image=[UIImage imageNamed:@"femaleIcon.png"];
        
    }
    else
    {
        //if we go to here it means the post don't know what is the user gender
        NSLog(@"post dosn't have user's gender");
        //set the pic to none
        //self.imgActivityUserGender.image=nil;
        
    }
}

-(NSString*) convertDistanceToText:(CLLocationDistance*) distance
{
    NSMutableString *textDistance =[NSMutableString stringWithFormat:@"%@",KeyDistanceMoreThenHour];
    
    //1 kilometer
    double smallWalk = 1000.0;
    //2 kilometer
    double bigWalk = 2000.0;
    //7 kilometer
    double bigDrive = 7000.0;
    
    
    
    double distanceInDouble = *distance;
    
    if( distanceInDouble<200)
    {
       
        NSString *distanceDescription=[NSString stringWithFormat:@"%i %@",1,KeyDistanceWalking];
        textDistance=[NSMutableString stringWithFormat:@"%@",distanceDescription];
    }
   
    else if( distanceInDouble<smallWalk && distanceInDouble>5)
    {
        float dist=distanceInDouble/2;
        NSString *distanceDescription=[NSString stringWithFormat:@"%.f %@",dist,KeyDistanceWalking];
        textDistance=[NSMutableString stringWithFormat:@"%@",distanceDescription];
    }
    else if (distanceInDouble<bigWalk)
    {
        float dist=distanceInDouble/2;
        NSString *distanceDescription=[NSString stringWithFormat:@"%.f %@",dist,KeyDistanceWalking];
        textDistance=[NSMutableString stringWithFormat:@"%@",distanceDescription];
    }
    else if (distanceInDouble<bigDrive)
    {
        float dist=distanceInDouble/400;
        NSString *distanceDescription=[NSString stringWithFormat:@"%.f %@",dist,KeyDistanceDriving];
        textDistance=[NSMutableString stringWithFormat:@"%@",distanceDescription];
    }
    
    
    
    
    
    return textDistance;
}

@end
