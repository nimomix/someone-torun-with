//
//  WIAppData.h
//  iTrade
//
//  Created by Ariel Peremen on 8/11/13.
//  Copyright (c) 2013 Wabbit. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"
#import <CoreLocation/CoreLocation.h>
#import "STRPost.h"
#import "MapLocation.h"

@interface AppData : NSObject <NSURLConnectionDataDelegate,UIAlertViewDelegate>

@property (strong, nonatomic) User *user;
@property (strong, nonatomic) User *currentUser;
@property (strong, nonatomic) NSString* currentFilterActivityType;
@property (strong, nonatomic) NSMutableArray* postsAnswered;


//function to create the single tone
+(AppData*)sharedInstance;

//Get current location
-(CLLocation*) getCurrentLocation;

-(NSString*) dateOrgenizer: (NSDate*) dateToOrgenize;
-(NSString*) convertDateToRelativeTime: (NSDate*) date;
-(NSString*) convertActivityTypeToHebrew :(NSString*) activityTypeName;
-(NSString*) convertActivityTypeToEnglish :(NSString*) activityTypeName;

-(void) loadDataFromDisk;
-(void) saveDataToDisk;

-(STRPost*)getPostFromDictionary:(NSDictionary*)dictionary;

-(void)getProfileImageFromServer :(NSString*)objectId andUiImage:(UIImageView*) viewForImage;


@end
