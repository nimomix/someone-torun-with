//
//  STRPostDetailsVC.h
//  Sporty
//
//  Created by נמרוד בר on 31/10/13.
//
//

#import <UIKit/UIKit.h>
#import "STRPost.h"
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "MapLocation.h"
#import "STRShowLocationVC.h"


@interface STRPostDetailsVC : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *imgActivityType;

@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UILabel *lblActivityDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblActivityType;
@property (weak, nonatomic) IBOutlet UILabel *lblCreatedAt;
@property (weak, nonatomic) IBOutlet UILabel *lblActivityTime;
//@property (weak, nonatomic) IBOutlet UILabel *lblUserPhoneNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblActivityLocation;
//@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) NSString *answeredPostObjectId;
@property (strong, nonatomic) NSDictionary* currentPost;
@property (strong, nonatomic) PFObject* PFObjectCurrentPost;
@property (weak, nonatomic) IBOutlet UISlider *sliderWorkoutLevel;

@property (weak, nonatomic) IBOutlet UIButton *btnContact;
@property (weak, nonatomic) IBOutlet UIImageView *imgProfilePicture;
@property (weak, nonatomic) IBOutlet UIImageView *imgUserGender;


//static words
@property (weak, nonatomic) IBOutlet UILabel *lblEasy;
@property (weak, nonatomic) IBOutlet UILabel *lblMedium;
@property (weak, nonatomic) IBOutlet UILabel *lblHard;

@end
