//
//  STRInboxVC.m
//  Sporty
//
//  Created by נמרוד בר on 18/11/13.
//
//

#import "STRInboxVC.h"
#import "STRInboxCell.h"
#import "RESideMenu.h"

@interface STRInboxVC ()
@property (strong, nonatomic) NSMutableDictionary *inboxDic;
@property (strong, nonatomic) NSMutableArray *sectionActivity;
@end

@implementation STRInboxVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.inboxDic = [[NSMutableDictionary alloc]init];
    self.sectionActivity = [NSMutableArray array];
    
    //check if we have a user first
    if([PFUser currentUser]!=nil)
       {
           NSString* objectID =  [PFUser currentUser].objectId;;
           [self getPostsByUser:objectID];
           //    [self ExecuteGetInboxData];
       }
    else
    {
        //if we don't have a user we will need to ask user to login or cancel
        
        //hide the table view
        self.myInboxTableView.hidden=YES;
        
        //create a button for login
        [self createLoginButton];
        
        
    }
    

    
#pragma mark - sideBar
    
    //set the left navigation item
	self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered target:self action:@selector(showMenu)];
    //set the gesture to bring the side menu
    UIPanGestureRecognizer *gestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandler:)];
    [self.view addGestureRecognizer:gestureRecognizer];

}

- (void)swipeHandler:(UIPanGestureRecognizer *)sender
{
    [[self sideMenu] showFromPanGesture:sender];
}

- (void)showMenu
{
    self.sideMenu.backgroundImage = [UIImage imageNamed:@"back.png"];
    [self.sideMenu show];
}



-(void) createLoginButton
{
    
    //create button for login in
    UIButton *btnLogin = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    //create the frame ( the 0 dosnt matter becuse we will center button in center of view)
    btnLogin.frame=CGRectMake(0, 0, 300, 300);
    //center it in view
    btnLogin.center =  self.view.center;
    [btnLogin addTarget:self
                           action:@selector(createLoginScreen)
                 forControlEvents:UIControlEventTouchUpInside];
    //set title
    [btnLogin setTitle:@"No inbox to show, \nbecuse no user is loged in \nWant to login now?" forState:UIControlStateNormal];
    [btnLogin setTitleColor: [UIColor blackColor] forState: UIControlStateNormal];
    
    
    btnLogin.titleLabel.textAlignment=NSTextAlignmentCenter;
    btnLogin.titleLabel.numberOfLines= 0;
    //add to view
    [self.view  addSubview:btnLogin];

}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"inboxCell";
    STRInboxCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"STRInboxCell" owner:self options:nil][0];
    }
    //if we git more then 0 posts we will send the post details to the cell class to handle it.
    
    if (self.sectionActivity.count != 0)
    {
    NSString *tempActivity = [self.sectionActivity objectAtIndex:indexPath.section];
    NSArray *tempArray = [self.inboxDic valueForKey:tempActivity];
        
    [cell SetCurrentPostInCell:[tempArray objectAtIndex:indexPath.row]];
        
    }
    else
    {
        //No post return from quary
        NSLog(@"There are 0 posts found" );
    }
    
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString *sectionName = [self.sectionActivity objectAtIndex:section];
    NSArray *rowInSectionArray = [self.inboxDic valueForKey:sectionName];
    return rowInSectionArray.count;
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.sectionActivity.count;
}
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [self.sectionActivity objectAtIndex:section];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    //create the VC which will present the post
    STRUserDetailsForContact* userDetailsForContactVC = [[STRUserDetailsForContact alloc]initWithNibName:nil bundle:nil];
    

    /////////////////
    
    //get the post that user has replayed for
    NSString *tempActivity = [self.sectionActivity objectAtIndex:indexPath.section];
    NSArray *tempArray = [self.inboxDic valueForKey:tempActivity];
    NSDictionary *currentPost =[tempArray objectAtIndex:indexPath.row];
   
    //get the answerd by object id and pass it to the vc
    userDetailsForContactVC.userDetails = currentPost[keyAnsweredBy];
    
    
    
    //////
    
    //push the post details VC
    [self.navigationController pushViewController:userDetailsForContactVC animated:YES];
  
    //deselacte the row
    [self.myInboxTableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(void)ExecuteGetInboxData
{
//    NSString* objectID =  [PFUser currentUser].objectId;;
//    NSThread* userPostsThread = [[NSThread alloc]initWithTarget:self selector:@selector(getPostsByUser:) object:objectID];
//    [userPostsThread start];
}
-(void)getPostsByUser:(NSString*)objectID
{
    
    //Get the answers table
    PFQuery *inboxQ = [PFQuery queryWithClassName:@"Answers"];
    
    //filter the post answered by current user
    [inboxQ whereKey:@"PostBy" equalTo:[PFUser currentUser]];
    
    //block to get the info
    [inboxQ findObjectsInBackgroundWithBlock:^(NSArray *posts, NSError *error) {
        
        if (!error) {
            // The find succeeded.
            NSLog(@"Successfully retrieved %lu scores.", (unsigned long)posts.count);
            // Do something with the found object
            for (PFObject *post in posts)
            {
                NSDictionary *answerDict = [post valueForKey:@"estimatedData"];
                
                //Get the answeredBy user Object Id
                PFUser* answeredByPFUser = post[keyAnsweredBy];
                
                //set the PFUser who answered in the post (dictionary)
                [answerDict setValue:answeredByPFUser forKey:keyAnsweredBy];
                
                
                NSString *activityTimeFinal = [[AppData sharedInstance] dateOrgenizer:[answerDict valueForKey:KeyActivityTime]];
                NSString *activityHeader = [NSString stringWithFormat:@"%@ - %@",[answerDict valueForKey:KeyActivityType],activityTimeFinal ] ;
                
                //Checks if inbox is empty and if so add a key to the inbox dict named on the activity type which will contain an array of answers(dictionaries)
                if (self.inboxDic.count == 0)
                {
                    NSMutableArray *newActivity = [NSMutableArray array];
                    [newActivity addObject:answerDict];
                    [self.inboxDic setObject:newActivity forKey:activityHeader];
                    [self.sectionActivity addObject:activityHeader];
                }
                //If the inbox is not empty - checks if the activity exist.
                else
                {
                    //if activity not exist - add a key as before.
                    if ([self.inboxDic valueForKey:activityHeader]==nil )
                    {
                        NSMutableArray *newActivity = [NSMutableArray array];
                        [newActivity addObject:answerDict];
                        [self.inboxDic setObject:newActivity forKey:activityHeader];
                        [self.sectionActivity addObject:activityHeader];
                        
                    }
                    //if activity exist - get the answers array and add another answer (dictionary)
                    else
                    {
                        NSMutableArray *existActivity = [self.inboxDic valueForKey:activityHeader];
                        [existActivity addObject:answerDict];
                    }
                    
                }
                
                
            }
        } else {
            // Log details of the failure
            NSLog(@"Error: %@ %@", error, [error userInfo]);
        }
        [self.myInboxTableView reloadData];
        
    }];
    
}

#pragma mark - Login \ Signup

-(void) createLoginScreen
{
    //call the login screen
        
    
    // Create the log in view controller
    PFLogInViewController *logInViewController = [[PFLogInViewController alloc] init];
    [logInViewController setDelegate:self]; // Set ourselves as the delegate
    
    // Create the sign up view controller
    STRSignUpVC *signUpViewController = [[STRSignUpVC alloc] init];
    [signUpViewController setDelegate:self]; // Set ourselves as the delegate
    
    //what fields should be in the signup
    [signUpViewController setFields:PFSignUpFieldsUsernameAndPassword | PFSignUpFieldsSignUpButton | PFSignUpFieldsDismissButton];
    
    // Assign our sign up controller to be displayed from the login controller
    [logInViewController setSignUpController:signUpViewController];
    
    // Present the log in view controller
    [self presentViewController:signUpViewController animated:YES completion:NULL];
}


/// Sent to the delegate when a PFUser is signed up.
- (void)signUpViewController:(PFSignUpViewController *)signUpController didSignUpUser:(PFUser *)user
{
    NSLog(@"we have a user %@", user);
    
    //now after we signed up the user we need to add his gender
    //get the signup VC
    STRSignUpVC *signUpVC = (STRSignUpVC*) signUpController;
    int female= 0;
    //check the selction in the segment
    if (signUpVC.maleFemaleSegmented.selectedSegmentIndex==female)
    {
        [[PFUser currentUser] setObject:keyGenderFemale forKey:keyGender];
        
    }
    else
    {
        //if it's not a female it's a male
        [[PFUser currentUser] setObject:keyGenderMale forKey:keyGender];
        
    }
    
    //save the gender in server
    [[PFUser currentUser] saveInBackground ];

    
    
    //dissmis the the login VC
    [signUpController dismissViewControllerAnimated:YES completion:nil];
    
    //Back to list VC
    [self.navigationController popViewControllerAnimated:YES];
    
}

/// Sent to the delegate when the sign up attempt fails.
- (void)signUpViewController:(PFSignUpViewController *)signUpController didFailToSignUpWithError:(NSError *)error
{
    NSLog(@"FAIL TO LOGIN");
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"You didn't complete the sighn up- try again" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    
    [alertView show];
    
    //dissmis the the login VC
    [signUpController dismissViewControllerAnimated:YES completion:nil];
    
}

// Sent to the delegate to determine whether the log in request should be submitted to the server.
- (BOOL)logInViewController:(PFLogInViewController *)logInController shouldBeginLogInWithUsername:(NSString *)username password:(NSString *)password {
    // Check if both fields are completed
    if (username && password && username.length && password.length) {
        return YES; // Begin login process
    }
    
    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Missing Information", nil) message:NSLocalizedString(@"Make sure you fill out all of the information!", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil] show];
    return NO; // Interrupt login process
}

// Sent to the delegate when a PFUser is logged in.
- (void)logInViewController:(PFLogInViewController *)logInController didLogInUser:(PFUser *)user {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

// Sent to the delegate when the log in attempt fails.
- (void)logInViewController:(PFLogInViewController *)logInController didFailToLogInWithError:(NSError *)error {
    NSLog(@"Failed to log in...");
}



@end
