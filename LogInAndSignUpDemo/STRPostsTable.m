//
//  STRPostsTable.m
//  SportyTest2
//
//  Created by נמרוד בר on 08/09/13.
//
//

#import "STRPostsTable.h"
#import "STRPostCell.h"
#import "STRAddNewPostVC.h"
#import "STRPostDetailsVC.h"
#import "STRPost.h"
#import "RESideMenu.h"

NSMutableArray* tempPostList;
int loadingCellTag;
BOOL noResults = NO;
@interface STRPostsTable () <CLLocationManagerDelegate>
@property (strong, nonatomic) STRPostCell* cellPost;
@property (strong, nonatomic) CLLocationManager* locationManager;
@property (strong, nonatomic) CLLocation* currentLocation;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray* posts;
@property (strong, nonatomic) NSMutableArray* PFObjectsPosts;
@property (strong, nonatomic) NSArray* sortedPosts;
@property (strong, nonatomic) UIButton *refrashListButton;
@property (strong, nonatomic) UIButton *btnCreatePost;
@property (strong, nonatomic) UIActivityIndicatorView *loadingCellActivityIndicator;

@end

@implementation STRPostsTable

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
    //set background color
    [self setScreenBackgroundColor];
    
   
    
    //add gusture for swiping to get menu
    [self addGestureRecognizer];
    
    
    //add buttons to nav bar
    [self addButtonsToNavBar];
    

    //set the status bar color to not transpernt
    [self setStatusbarNotTranslocent];
    
    
}
-(void)viewWillAppear:(BOOL)animated
{
    NSLog(@"wiil appear");
    //set self for delegate and init array
     [self setTableDelagteAndInitArray];

    //clear the nav bar title
    [self setNavBarTitleToNothing];
    
    //filter query result to all activity if no activity choosen
    [self filterQueryResults];
    
    //load data from disc
    [[AppData sharedInstance] loadDataFromDisk];
    
    //quary posts by loctailn
    [self GetPostsByLocation];
}

-(void)setScreenBackgroundColor
{
    self.view.backgroundColor=COLORBackgroundWhite;
}

-(void) setStatusbarNotTranslocent
{
if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
self.edgesForExtendedLayout = UIRectEdgeNone;
}

-(void)setNavBarTitleToNothing
{
    //make title to Nothing
    self.title = @"";
   
}

-(void)setNavBarTitleToBack
{
    //make title to Nothing
    self.title = @"חזרה";
}

-(void)addGestureRecognizer
{
    UIPanGestureRecognizer *gestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandler:)];
    [self.view addGestureRecognizer:gestureRecognizer];
}

-(void) setTableDelagteAndInitArray
{
    //self.tableView.tableHeaderView.backgroundColor= [UIColor redColor];
    
    //set self to delegate on table
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    
    //init Posts Array
    self.posts = [NSMutableArray array];
    self.PFObjectsPosts = [NSMutableArray array];
    loadingCellTag = 10;
}

-(void) filterQueryResults
{
//    //show all posts if no selction made
//    if ([[AppData sharedInstance].currentFilterActivityType isEqual:@"ALL"])
//    {
//        //make title to Nothing
//        self.title = @"qqqqqq";
//    }
}

-(void) addButtonsToNavBar
{
    
    //make a button
    UIButton *addButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //set the size
    addButton.frame = CGRectMake(0, 0, 90 , 30);
    //set title
    [addButton setTitle:@"פרסם" forState:UIControlStateNormal];
    //set font
    addButton.titleLabel.font=FONTAlefBold22;
    //set the button image
    [addButton setBackgroundImage:[UIImage imageNamed:@"red_tab.png"] forState:UIControlStateNormal];
    //add selctor to button
    [addButton addTarget:self action:@selector(newPostButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    //add the button to a uibarbutton icon
    UIBarButtonItem *addBarButton = [[UIBarButtonItem alloc] initWithCustomView:addButton] ;
    //add the uibarbutton to the navbar
    self.navigationItem.rightBarButtonItem = addBarButton;
    
    
    
    //make a button
    UIButton *menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //set the size
    menuButton.frame = CGRectMake(0, 0, 24 , 20);
    //set the button image
    [menuButton setBackgroundImage:[UIImage imageNamed:@"menuIcon.png"] forState:UIControlStateNormal];
    //add selctor to button
    [menuButton addTarget:self action:@selector(showMenu) forControlEvents:UIControlEventTouchUpInside];
    //add the button to a uibarbutton icon
    UIBarButtonItem *menuBarButton = [[UIBarButtonItem alloc] initWithCustomView:menuButton] ;
    //add the uibarbutton to the navbar
    self.navigationItem.leftBarButtonItem=menuBarButton;

}


- (void)swipeHandler:(UIPanGestureRecognizer *)sender
{
    [[self sideMenu] showFromPanGesture:sender];
}


#pragma mark Button actions

- (void)showMenu
{
    self.sideMenu.backgroundImage = [UIImage imageNamed:@"back.png"];
    [self.sideMenu show];
}


-(void) GetPostsByLocation
{
    //reset the loading cell - means we reloading the table view and loading cell need to be reset
    [self resetLoadingCell];
    
    
    //Get the current location of the user to show the close-Locations Posts
    if (self.locationManager == nil)
    {
        //init location manager
        self.locationManager = [[CLLocationManager alloc]init];
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest ;
        //ios 8 fix for getting user approvel on location
        [self.locationManager requestWhenInUseAuthorization];
        
    }
    
    //start getting updates
    [self.locationManager startUpdatingLocation];
    self.locationManager.delegate = self;
    
    
    //get location from location manger
    self.currentLocation = self.locationManager.location;
    //NSLog(@"Current location: %@",self.currentLocation);

    //create a refrash button
    if(self.refrashListButton==nil)
    {
        
        self.refrashListButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        NSLog(@"No location yet- need to refrash");
        
        [self.refrashListButton addTarget:self
                                   action:@selector(refreshBtnTapped)
                         forControlEvents:UIControlEventTouchDown];
        
        [self.refrashListButton setTitle:@"Refresh"
                                forState:UIControlStateNormal];
        
        self.refrashListButton.frame = CGRectMake(80.0, 210.0, 160.0, 40.0);
        
    }


    

    
    //if we have a location we will make a query
    if(self.currentLocation!=nil)
    {
        //hide the refrash button
        self.refrashListButton.hidden=YES;
        [self.refrashListButton removeFromSuperview];
        [self.view addSubview:self.refrashListButton];
        //make the list in the back
        self.tableView.alpha=0.7;
        //show the refrash button
       // self.refrashListButton.hidden=NO;
        
        PFQuery *locQuery =[self createQueryByLocationOnPosts:self.currentLocation];
        
        //Create thread to run the quary in the back, get all the objects into objects array
        
        [self runQueryOnBackThread:locQuery];
        
       //make the list in the back
        self.tableView.alpha=1;


        
    }
    //if we don't have geo location yet we cant run a qiury with null
    else
    {

        
        [self.view addSubview:self.refrashListButton];
        
        //make the list in the back
        self.tableView.alpha=0.7;
        //show the refrash button
        self.refrashListButton.hidden=NO;
        
    }
    
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    if(self.currentLocation==nil)
    {
        //get the last location from array
        self.currentLocation = locations.lastObject;
        
        //now that we have a location we can run the query
        [self GetPostsByLocation];
        
    }
}

-(void)refreshBtnTapped
{
    [self GetPostsByLocation];
}

-(void)runQueryOnBackThread:(PFQuery*) query
{
#pragma mark - BLOCK
    tempPostList = [[NSMutableArray alloc]initWithArray:self.posts];
    
    //NSLog(@"Location: %@" ,self.currentLocation);
    [query setSkip:loadingCellTag-10];
    [query setLimit:10];
    //Create thread to run the quary in the back, get all the objects into objects array
    [query findObjectsInBackgroundWithBlock:^(NSArray *postsArray, NSError *error) {
        if (!error) {
            // The search succeeded.
            NSLog(@"Successfully retrieved %lu scores.", (unsigned long)postsArray.count);
            
            //if we have more then 0 results
            if (postsArray.count>0)
            {
                // get each object from the array into post dictionary
                for (PFObject *postDetails in postsArray)
                {
                    //using parse key-"estimateData" we convert the PFobject to readble dictionary.
                    NSMutableDictionary *postDict = [postDetails valueForKey:KeyEstimatedData];
                    
                    //get the time of creation for the post
                    NSDate *postCreatedAt = [postDetails valueForKey:KeyCreatedAt];
                    
                    
                    //get the object id
                    NSString *postObjectId = postDetails.objectId;
                    
                    //add the post object id to the the dict
                    [postDict setObject:postObjectId forKey:KeyObjectId];
                    
                    //add the creation time of the post to the post
                    [postDict setObject:postCreatedAt forKey:KeyCreatedAt];
                    
                    //Add the dictitionary object to posts array
                    [tempPostList addObject:postDict];
                    
                    //Add the PFObject to an array
                    [self.PFObjectsPosts addObject:postDetails];
                }
        
                
                self.posts = [[NSMutableArray alloc]initWithArray:tempPostList];
                
                
                //Reload data when the block is finished
                //[self sortArraybyPostDate];
                [self.tableView reloadData];
                
                //hide the cell if no more than 10 results - meaning no following request
                if (postsArray.count < 10)
                {
                    noResults = YES;
                }
               
            }
            else
            {
                //if there are no results we will stop the activity indicator
                
                [self hideLoadingCell];
                noResults = YES;
            }
            
           
            
        }
        else
        {
            // Log details of the failure
            NSLog(@"No results Error: %@ %@", error, [error userInfo]);
        }
        
    }
     
     ];
    
    
}
-(void)resetLoadingCell
{
    //show and start animating activity indicator
    [self.loadingCellActivityIndicator startAnimating];
    self.loadingCellActivityIndicator.hidden=NO;
    
    //reset no results state
    noResults = NO;
    
    //hide create post button
    self.btnCreatePost.hidden=YES;
}
-(void)hideLoadingCell
{
    //hide and stop activity indicator
    [self.loadingCellActivityIndicator stopAnimating];
    self.loadingCellActivityIndicator.hidden=YES;
    
    //show create post button
    self.btnCreatePost.hidden=NO;

}

-(PFQuery*)createQueryByLocationOnPosts:(CLLocation*) location
{
    //Get the current loction of user
    PFGeoPoint *currentGeoPoint = [PFGeoPoint new];
    currentGeoPoint.latitude = location.coordinate.latitude;
    currentGeoPoint.longitude = location.coordinate.longitude;
    
    
    
    
    //Create quary on the class- Posts
    PFQuery *locQuery = [PFQuery queryWithClassName:KeyClassPosts];
    

    
    //filters the currentuser posts
    
    //If there is a user
    if ([PFUser currentUser])
    {
        //dont get his posts - becuse i don't need to see my posts
          //[locQuery whereKey:@"User" notEqualTo:[PFUser currentUser]];
    }
  
    // Searching for post near the current location by key -"location"
    [locQuery whereKey:KeyActivityLocation nearGeoPoint:currentGeoPoint withinKilometers:RadiosForPostSearch];

    
    if (![[AppData sharedInstance].currentFilterActivityType isEqual:@"ALL"])
    {
        [locQuery whereKey:KeyActivityType equalTo:[AppData sharedInstance].currentFilterActivityType];
    }
    
    //filter posts older then today
    NSDate *today= [NSDate dateWithTimeIntervalSinceNow:0];
    [locQuery whereKey:KeyActivityTime greaterThan:today];
    
    //sort results - closest date first
    [locQuery orderByAscending:KeyActivityTime];
    
    //[locQuery addDescendingOrder:KeyActivityLocation];
    
     
    
    return locQuery;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)sortArraybyPostDate
{
    //Making a temp array for sorting
    self.sortedPosts = [NSArray array];
    
    //sorting by date
    self.sortedPosts = [self.posts sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *dict2, NSDictionary *dict1) {
        return [[dict1 objectForKey:KeyActivityTime] compare:[dict2 objectForKey:KeyActivityTime]];
    }];
    
    //convert to mutable array for placing back in original array
    self.posts = (NSMutableArray*) self.sortedPosts;
    
}


//Table view Delegate methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
 
    //add + 1 because of the activity cell
    return self.posts.count+1;
    
}
- (UITableViewCell *)postCellForIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellPost";
    STRPostCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"STRPostCell" owner:self options:nil][0];
    }
    if (self.posts.count != 0)
    {
        //if we got more then 0 posts we will send the post details to the cell class to handle it.
   
        [cell SetCurrentPostInCell:[self.posts objectAtIndex:indexPath.row]];
        
    }
    else
    {
        //No post return from quary
        NSLog(@"There are 0 posts found" );
    }
    
    //when selcting a cell don't color it in gray
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //if we have the cell to desplay is smaller then the array we have
    if (indexPath.row < self.posts.count)
    {
        //creat the post cell
        return [self postCellForIndexPath:indexPath];
    }
    else
    {
        //ask for more cell
        return [self loadingCell];
    }
}

//paging
- (UITableViewCell *)loadingCell
{
    //create the loading cell
    UITableViewCell *loadingCell = [[UITableViewCell alloc]
                             initWithStyle:UITableViewCellStyleDefault
                             reuseIdentifier:nil];
    
    
    if(self.loadingCellActivityIndicator==nil)
    {
        //create activity indicator
        self.loadingCellActivityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        self.loadingCellActivityIndicator.color = [UIColor redColor];
    }
        self.loadingCellActivityIndicator.center =  loadingCell .center;
        [loadingCell  addSubview:self.loadingCellActivityIndicator];
        [self.loadingCellActivityIndicator startAnimating];
        
        //create button to create post to be at end of the list
        self.btnCreatePost = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        self.btnCreatePost.frame=CGRectMake(loadingCell.frame.origin.x+10, loadingCell.frame.origin.y+10, 300, 90);
        [self.btnCreatePost addTarget:self
                           action:@selector(newPostButtonTapped)
                 forControlEvents:UIControlEventTouchUpInside];
    
        //set its title
        //self.btnCreatePost.titleLabel.text=@"";
    //self.btnCreatePost.lineBreakMode= NSLineBreakByWordWrapping;
    
    
    [self.btnCreatePost.titleLabel setFont:FONTAlefRegular17];
    [self.btnCreatePost setTitle:KeyNoPartnersCell forState:UIControlStateNormal];
    [self.btnCreatePost setTitleColor: COLORRed forState: UIControlStateNormal];
    
        self.btnCreatePost.titleLabel.textAlignment=NSTextAlignmentCenter;

    
        //hide it until no result will come from qury
        self.btnCreatePost.hidden=YES;

    
        //set the background color to clear
        loadingCell.backgroundColor=[UIColor clearColor];
        //add it to cell
        [loadingCell  addSubview:self.btnCreatePost];

    
    return  loadingCell;
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //if we will about to display last of cells -3
    if (indexPath.row+1== loadingCellTag-3)
    {
        //load next 10 posts
        loadingCellTag = loadingCellTag + 10;
        [self GetPostsByLocation];
    }
    else if(indexPath.row == self.posts.count) //if last row by current number of posts - check if there is more results or not
    {
        if (noResults)
        {
            [self hideLoadingCell];
        }
        
    }

}

- (void)newPostButtonTapped
{
    //create the new activity edit VC
    STRAddNewPostVC *newPostVC = [[STRAddNewPostVC alloc]initWithNibName:nil bundle:nil];
    //set the title of this page for the back button on next screen
    [self setNavBarTitleToBack];
    //push the activty edit vc
    [self.navigationController pushViewController:newPostVC animated:YES];
}
- (void)viewDidUnload
{
    [self setTableView:nil];
    [super viewDidUnload];
    
}
//Hight of the cell
-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 101;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //deselacte the row
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    //NSLog(@"%lu",(unsigned long)[AppData sharedInstance].postsAnswered.count);

    //create the VC which will present the post
    STRPostDetailsVC* postDetails = [[STRPostDetailsVC alloc]initWithNibName:nil bundle:nil];
    
    //checks if already answered and send info to post Details VC
    STRPostCell* cell = (STRPostCell*)[tableView cellForRowAtIndexPath:indexPath];

    //we check if this is an activty type cell
    if ([cell isKindOfClass:[STRPostCell class]])
    {
        //this is a regular cell
        if (cell.answerObjectId!=nil)
        {
            NSLog(@"2nd :%@", cell.answerObjectId);
            postDetails.answeredPostObjectId=cell.answerObjectId;
            
           
        }
         //set the post details for the presented VC
        postDetails.currentPost = [self.posts objectAtIndex:indexPath.row];
        
        //set the current PFObject post details for the next screen
        for (PFObject* post in self.PFObjectsPosts) {
            if ([post.objectId isEqualToString:[postDetails.currentPost valueForKey:KeyObjectId]]) {
                postDetails.PFObjectCurrentPost = post;
            }
        }
        

        //set the title of this page for the back button on next screen
        [self setNavBarTitleToBack];
        //push the post details VC
        [self.navigationController pushViewController:postDetails animated:YES];
        
    }
    
    

}



-(void)logOutButtonTapAction{
//    [PFUser logOut];
//    // Create the log in view controller
//    PFLogInViewController *logInViewController = [[PFLogInViewController alloc] init];
//    [logInViewController setDelegate:self]; // Set ourselves as the delegate
//    
//    // Create the sign up view controller
//    PFSignUpViewController *signUpViewController = [[PFSignUpViewController alloc] init];
//    [signUpViewController setDelegate:self]; // Set ourselves as the delegate
//    
//    // Assign our sign up controller to be displayed from the login controller
//    [logInViewController setSignUpController:signUpViewController];
//    
//    //     Present the log in view controller
//    [self presentViewController:logInViewController animated:YES completion:NULL];
    
    
}

//#pragma mark - Login class
////************************************ log in classes ************************************
//
//#pragma mark - PFLogInViewControllerDelegate
//
//// Sent to the delegate to determine whether the log in request should be submitted to the server.
//- (BOOL)logInViewController:(PFLogInViewController *)logInController shouldBeginLogInWithUsername:(NSString *)username password:(NSString *)password {
//    // Check if both fields are completed
//    if (username && password && username.length && password.length) {
//        return YES; // Begin login process
//    }
//    
//    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Missing Information", nil) message:NSLocalizedString(@"Make sure you fill out all of the information!", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil] show];
//    return NO; // Interrupt login process
//}
//
//// Sent to the delegate when a PFUser is logged in.
//- (void)logInViewController:(PFLogInViewController *)logInController didLogInUser:(PFUser *)user {
//    [self dismissViewControllerAnimated:YES completion:NULL];
//}
//
//// Sent to the delegate when the log in attempt fails.
//- (void)logInViewController:(PFLogInViewController *)logInController didFailToLogInWithError:(NSError *)error {
//    NSLog(@"Failed to log in...");
//}
//
//// Sent to the delegate when the log in screen is dismissed.
//- (void)logInViewControllerDidCancelLogIn:(PFLogInViewController *)logInController {
//    [self.navigationController popViewControllerAnimated:YES];
//}




@end
