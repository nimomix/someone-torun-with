//
//  STRLoginVC.h
//  Sporty
//
//  Created by gonzo bonzo on 4/7/14.
//
//

#import <UIKit/UIKit.h>

@interface STRSignUpVC : PFSignUpViewController <PFLogInViewControllerDelegate, PFSignUpViewControllerDelegate>

@property (nonatomic, retain)  UISegmentedControl *maleFemaleSegmented;

@end
