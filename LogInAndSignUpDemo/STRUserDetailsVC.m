//
//  STRUserDetailsVC.m
//  Sporty
//
//  Created by נמרוד בר on 10/11/13.
//
//

#import "STRUserDetailsVC.h"
#import "STRMyPostsCell.h"
#import "RESideMenu.h"
#import <QuartzCore/QuartzCore.h>
#import "STRAddNewPostVC.h"

@interface STRUserDetailsVC ()
@property (strong, nonatomic)NSMutableArray* myPostsArray;
@end

@implementation STRUserDetailsVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    



    
    //set nav bar items
    [self setNavBarDetails];

    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshTable) name:@"post_updated" object:nil];
   
    //set the menu will slide in when swipe from the side
    [self setSwipeGestureRecognizer];

}
-(void)viewWillAppear:(BOOL)animated
{
    //check if user is logged in
    if ([PFUser currentUser]!= nil)
    {
        //get user posts list
        [self ExecuteGetPostsByUser];
        
        //set user details
        [self setUserDetails];
    }
    else
    {
        //no user is logged in
        [self setNoUserIsLogedInScreen];
    }
    
}

-(void)setNoUserIsLogedInScreen
{
    //no details available - USER IS NOT REGISTERED
    UILabel* noDetails = [[UILabel alloc]initWithFrame:[UIScreen mainScreen].bounds];
    noDetails.text = @"No Personal Details Avialable";
    noDetails.backgroundColor = [UIColor blackColor];
    noDetails.alpha = 0.80;
    noDetails.textColor = [UIColor whiteColor];
    noDetails.textAlignment = NSTextAlignmentCenter;
    noDetails.font = [UIFont boldSystemFontOfSize:22];
    [self.view addSubview:noDetails];
    [self.view setUserInteractionEnabled:NO];
}

-(void)setUserDetails
{
    //init user posts array
    self.myPostsArray = [NSMutableArray array];
    //get the user
    PFUser *user = [PFUser currentUser];
    


    
    
    //set the user details in the labels
    //set the user name
    self.lblUserName.text=user.username;
    //set the user phone
    NSDictionary *serverData =[user valueForKey:KeyServerData];
    NSString *phone = [serverData valueForKey:KeyPhoneNumber];
    //set the pgone number in label
    self.lblMyPhone.text = phone;
    
    
    //set circale button on male or female
    //get the gender from user class
    NSString *gender =[serverData valueForKey:keyGender];

    if([gender isEqualToString:keyGenderFemale])
    {
        //if the string is a female- set the button circle on the female picture
        self.btnMaleFemaleCircle.frame=CGRectMake(101, 220, 50, 50);
        
    }
    else
    {
        //if the string is a male- set the button circle on the male picture
        self.btnMaleFemaleCircle.frame=CGRectMake(179, 220, 50, 50);
    }
    
    //get user profile picture
    PFFile *profilePicturePFFile = [serverData valueForKey:keyProfilePicture];
    //check if user has profile image in server
    if(profilePicturePFFile!=nil)
    {
    [self getProfilePhotoFromServer:profilePicturePFFile];
    }
    

    

}

-(void)setNavBarDetails
{
    //set the left navigation item
	self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered target:self action:@selector(showMenu)];
}



-(void)setSwipeGestureRecognizer
{
    //set the gesture to bring the side menu
    UIPanGestureRecognizer *gestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandler:)];
    [self.view addGestureRecognizer:gestureRecognizer];
}

-(void)ExecuteGetPostsByUser
{
    NSString* objectID =  [PFUser currentUser].objectId;;
     NSThread* userPostsThread = [[NSThread alloc]initWithTarget:self selector:@selector(getPostsByUser:) object:objectID];
    [userPostsThread start];
}
-(void)getPostsByUser:(NSString*)objectID
{
    //Reset the Array
    self.myPostsArray = [NSMutableArray array];
    //create query on posts class
    PFQuery *query = [PFQuery queryWithClassName:KeyClassPosts];
    
    //look under the user key for the same key from current user
    [query whereKey:KeyClassUser equalTo:[PFUser currentUser]];
    
    //get the array of objects
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            // The find succeeded.
            NSLog(@"Successfully retrieved %lu scores.", (unsigned long)objects.count);
            // Do something with the found object
            for (PFObject *object in objects) {
                
                [self.myPostsArray addObject:object];
                
                
            }
        } else {
            // Log details of the failure
            NSLog(@"Error: %@ %@", error, [error userInfo]);
        }
        [self.MyPostsTableView reloadData];
        
    }];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)refreshTable
{
    self.myPostsArray = [NSMutableArray array];
    [self ExecuteGetPostsByUser];
}
#pragma mark - TableView Delegate
//Table view Delegate methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.myPostsArray.count;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellPost";
    STRMyPostsCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"STRMyPostsCell" owner:self options:nil][0];
    }
    if (self.myPostsArray.count != 0)
    {
        //if we git more then 0 posts we will send the post details to the cell class to handle it.
        [cell SetCurrentPostInCell:[[self.myPostsArray objectAtIndex:indexPath.row]valueForKey:KeyEstimatedData]];
    }
    else
    {
        //No post return from quary
        NSLog(@"There are 0 posts found" );
    }
    
    return cell;
}


//Hight of the cell
-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    //create the VC which will present the post
    STRAddNewPostVC* postEdit = [[STRAddNewPostVC alloc]initWithNibName:nil bundle:nil];

    //set the post details for the presented VC
    [postEdit setEditModeWithPost:[self.myPostsArray objectAtIndex:indexPath.row]];
    
    //push the post details VC
    [self.navigationController pushViewController:postEdit animated:YES];
    
    //deselacte the row
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)showMenu
{
    self.sideMenu.backgroundImage = [UIImage imageNamed:@"back.png"];
    [self.sideMenu show];
}

- (void)swipeHandler:(UIPanGestureRecognizer *)sender
{
    [[self sideMenu] showFromPanGesture:sender];
}
    
// Change picture of user

- (IBAction)userPictureTapped:(id)sender
{
    [self showActionSheet];

}


// Bring action sheet with 3 options.

-(void) showActionSheet
{
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Select picture option:" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                            @"From Existing photos",
                            @"From Our Gallery",
                            @"Take a picture",
                            nil];
    popup.tag = 1;
    [popup showInView:[UIApplication sharedApplication].keyWindow];
}
- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (popup.tag) {
        case 1: {
            switch (buttonIndex) {
                case 0:
                    [self getPhotoFromExisting];
                    break;
                case 1:
                   //from our existing libery
                    break;
                case 2:
                     [self getPhotoFromCamera];
                    break;
             
                default:
                    break;
            }
            break;
        }
        default:
            break;
    }
}

// get photo from the user exisitng libery
-(void) getPhotoFromExisting
{
    UIImagePickerController *pickerController = [[UIImagePickerController alloc]
                                                 init];
    pickerController.delegate = self;
    [self presentViewController:pickerController animated:YES completion:nil];
}

//get a photo using the camera
-(void)getPhotoFromCamera
{
    // Check for camera
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] == YES)
    {
        
        // Create image picker controller
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        
        // Set source to the camera
        imagePicker.sourceType =  UIImagePickerControllerSourceTypeCamera;
        
        // Delegate is self
        imagePicker.delegate =self;
        
        // Show image picker
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
    else
    {
        //alert user that there is no camera
        NSLog(@"No camera - no upload!");
        UIAlertView *noCameraAlert =[[UIAlertView alloc]initWithTitle:@"Sorry" message:@"No camera" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        //show alert
        [noCameraAlert show];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    
    // Access the uncropped image from info dictionary
    UIImage *image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    
    // Dismiss controller
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    
    if(image!=nil)
    {
        // Upload image
        NSData *imageData = UIImageJPEGRepresentation(image, 0.05f);
        [self uploadImage:imageData];
    }

    
}
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    
    // Dismiss controller
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}


- (void)uploadImage:(NSData *)imageData
{
    //create a pfile from the data
    PFFile *imageFile = [PFFile fileWithName:@"Image.jpg" data:imageData];
    //get the current user
    PFUser *user = [PFUser currentUser];
    //set the image to the user
    user[keyProfilePicture]=imageFile;
    
    //save the new image in the user class
    [user saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error)
                 {
                     if (!error)
                     {
                         //save photo has secsseded
     
                         //convert nsdata to image file
                         UIImage *image =[[UIImage alloc]initWithData:imageData];
     
                         //set the pic in the button
                         [self setUserPhotoInButton:image];
                     }
                     else
                     {
                         // Log details of the failure
                         NSLog(@"Error: %@ %@", error, [error userInfo]);
     
                         //alert user picture wasn't saved
                         UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Oops" message:@"We couldn't upload your picture. Try again." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                         [alert show];
                     }

                 }];

    
    
}

// put the image in the user picture button
- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo
{

    
    //convert image to data
    NSData *imageData=UIImageJPEGRepresentation(image, 0);
    //upload image to user (this will also update the button image when upload is done)
    [self uploadImage:imageData];
    
    
    //dissmiss the image picker
    [self dismissModalViewControllerAnimated:YES];
}
-(void)setUserPhotoInButton :(UIImage *)image
{
    self.BTNUserImage.layer.cornerRadius=43.0f;
    self.BTNUserImage.clipsToBounds=YES;
    //self.BTNUserImage.contentMode =UIViewContentModeScaleAspectFit;
    [self.BTNUserImage setImage:image forState:UIControlStateNormal];
}

-(void)getProfilePhotoFromServer: (PFFile*) profilePicturePFFile
{
    [profilePicturePFFile getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
        if (!error)
        {
            UIImage *image = [UIImage imageWithData:data];
            
            [self setUserPhotoInButton:image];
            
        }
    }];
}




@end
