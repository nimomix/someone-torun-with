//
//  STRInboxVC.h
//  Sporty
//
//  Created by נמרוד בר on 18/11/13.
//
//

#import <UIKit/UIKit.h>
#import "STRSignUpVC.h"
#import "STRUserDetailsForContact.h"

@interface STRInboxVC : UIViewController <UITableViewDataSource, UITableViewDelegate, PFLogInViewControllerDelegate, PFSignUpViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *myInboxTableView;

@end
