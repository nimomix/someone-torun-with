//
//  STRShowLocationVC.h
//  Sporty
//
//  Created by gonzo bonzo on 8/24/14.
//
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "MapLocation.h"

@interface STRShowLocationVC : UIViewController <MKMapViewDelegate, UIGestureRecognizerDelegate>


@property (strong, nonatomic) IBOutlet MKMapView *mapView;

@property (strong,nonatomic) CLLocation *currentLocation;
@property (strong, nonatomic) NSMutableArray *matchingItems;

@end
