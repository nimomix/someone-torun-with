//
//  STRAddNewPostVC.h
//  SportyTest2
//
//  Created by נמרוד בר on 08/09/13.
//
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "MapLocation.h"
#import "STRPost.h"
#import "STRSignUpVC.h"

@interface STRAddNewPostVC : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *btnForActivityDateText;


@property (weak, nonatomic) IBOutlet UITextField *lblActivityDescreption;
- (IBAction)txtEditingDone:(UITextField *)sender;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@property (weak, nonatomic) IBOutlet UITextView *activtyDetails;

@property (weak, nonatomic) IBOutlet UISlider *sliderWorkoutLevel;
@property (weak, nonatomic) IBOutlet UIButton *btnChangeDate;
//@property (weak, nonatomic) IBOutlet UIButton *btnSetDate;
@property (weak, nonatomic) IBOutlet UIView *datePickerView;
@property (weak, nonatomic) IBOutlet UIButton *btnChooseActivityType;
@property (weak, nonatomic) IBOutlet UILabel *lblActivityLevelEasy;
@property (weak, nonatomic) IBOutlet UILabel *lblActivityLevelMedum;
@property (weak, nonatomic) IBOutlet UILabel *lblActivityLevelHard;




@property (weak, nonatomic) IBOutlet UIButton *btnForActivityTypeText;
@property (strong, nonatomic) IBOutlet UIView *viewChooseActivity;




- (IBAction)btnChooseActivity:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnForActivityLocationText;

@property (nonatomic) BOOL inEditMode;
@property (nonatomic, strong) NSDate* currentPostTime;
@property (nonatomic, strong) NSDate* currentActivityTime;

@property (strong,nonatomic) CLLocation *currentLocation;
@property (nonatomic) BOOL didGotManualLocation;
-(void)setEditModeWithPost:(STRPost*)post;
- (IBAction)editBegan:(UITextField *)sender;
- (IBAction)EditEnd:(UITextField *)sender;
@end
